package search;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Login_Activity;
import com.brainmagic.adherebond.ProjectActivity;
//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.SearchList_Activity;
import com.brainmagic.adherebond.Welcome_Activity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import Logout.logout;
import about.About_Activity;
import adapter.AutoCompleteAdapter;
import adapter.AutoCompleteAdapterOthers;
import alertbox.Alertbox;
import alertbox.LoginAlert;
import alertbox.PasswordChangeAlert;
import contact.Contact_Activity;
import database.DBHelper;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import product.Product_Activity;
import whatsnew.Whatsnew_Activity;

public class Search_Activity extends AppCompatActivity {
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String usertype;
    NetworkConnection network = new NetworkConnection(Search_Activity.this);
    Alertbox alert = new Alertbox(Search_Activity.this);
    Spinner Searchspinner;
    ImageView back,home,menu;
    EditText search_text;
    String Searchstring,searchtype;
    ArrayList<String> SearchtypeList;
    ImageView Search_button;
    private AutoCompleteTextView autoCompleteTextView;
    AutoCompleteAdapter partnameadapter;
    AutoCompleteAdapterOthers autoCompleteAdapterOthers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_);
        myshare = getSharedPreferences("Registration",Search_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autotextview);
        home=(ImageView)findViewById(R.id.home);
        back=(ImageView)findViewById(R.id.back);
        search_text=(EditText)findViewById(R.id.searchtext);
        Searchspinner = (Spinner)findViewById(R.id.searchspinner);
        SearchtypeList = new ArrayList<String>();
        SearchtypeList.add("Select type");
        SearchtypeList.add("Product Name");
        SearchtypeList.add("Others");
        ArrayAdapter partnoadapter = new ArrayAdapter(Search_Activity.this, R.layout.simple_spinner_item, SearchtypeList);
        partnoadapter.setNotifyOnChange(true);
        Searchspinner.setAdapter(partnoadapter);
        Search_button=(ImageView)findViewById(R.id.search_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu=(ImageView)findViewById(R.id.menubar);

        TextView mTitle_text = findViewById(R.id.title_text);
        mTitle_text.setText("Search Products");

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Search_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub
                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Search_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(Search_Activity.this, Product_Activity.class);
                                product.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                product.putExtra("a","");
                                startActivity(product);
                                return true;
                            case R.id.expense:
                                PasswordChangeAlert alerts = new PasswordChangeAlert(Search_Activity.this);// new saleslogin().execute();
                                alerts.showLoginbox();
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Search_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Search_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.watsnew:
                                if (network.CheckInternet()) {
                                    Intent f = new Intent(Search_Activity.this, ProjectActivity.class);
                                    startActivity(f);}
                                else{
                                    Alertbox alert = new Alertbox(Search_Activity.this);
                                    alert.showAlertbox("Kindly check your Internet Connection");
                                }
                                return true;
                            case R.id.offers:
                                usertype=myshare.getString("Usertype","");
                                if(usertype.equals("Cera Employee"))
                                {
                                    if (!myshare.getBoolean("islogin",false))
                                    {
                                        CheckInternet();
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("alertlogin",false)));
                                    }
                                    else
                                    {
                                        if (network.CheckInternet()) {
                                            Intent f = new Intent(Search_Activity.this, Offers_Activity.class);
                                            startActivity(f);
                                        }
                                        else {

                                            alert.showAlertbox("Kindly check your Internet Connection");
                                        }

                                    }
                                }
                                else
                                {
                                    Intent f = new Intent(Search_Activity.this, Offers_Activity.class);
                                    startActivity(f);
                                }return true;



                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {
                                    //menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(Search_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {

                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                    ///menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(Search_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Search_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_search);

                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        Search_button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Searchstring = autoCompleteTextView.getText().toString();
                searchtype = Searchspinner.getSelectedItem().toString();
                if(searchtype.equals("Select type"))
                {
                    Toast.makeText(getApplicationContext(),"Select Search type",Toast.LENGTH_SHORT).show();
                    //search_text.setError("Cannot be empty");
                }else if(Searchstring.equals(""))
                {
                    search_text.setError("Cannot be empty");
                }
                else
                {
                    Intent i= new Intent(Search_Activity.this,SearchList_Activity.class);
                    i.putExtra("Searchtype",searchtype);
                    i.putExtra("Searchcontent",Searchstring);
                    startActivity(i);
                }

            }
        });


        search_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a=new Intent(Search_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        Searchspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                searchtype = adapterView.getItemAtPosition(i).toString();
                if (searchtype.equals("Product Name")) {
                    autoCompleteTextView.setThreshold(1);
                    autoCompleteTextView.setAdapter(partnameadapter);

                } else if (searchtype.equals("Others")){
                    autoCompleteTextView.setThreshold(1);
                    autoCompleteTextView.setAdapter(autoCompleteAdapterOthers);
                }
                else {
                    autoCompleteTextView.setAdapter(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        new GetPartsForAutotext().execute();
        new GetAllPartsForAutotext().execute();

    }
    public class GetAllPartsForAutotext extends AsyncTask<String, Void, String>
    {
        SQLiteDatabase db2;
        private ArrayList<String> partsNameList2 = new ArrayList<String>();;
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try
            {

                DBHelper dbHelper = new DBHelper(Search_Activity.this);
                db2 = dbHelper.readDataBase();

                String query ="select distinct MProdsub_name, Mprod_name from Product_details";
                Log.v("productname for autotex",query);
                Cursor cursor = db2.rawQuery(query, null);
                if(cursor.moveToFirst())
                {
                    do
                    {
                        partsNameList2.add(cursor.getString(cursor.getColumnIndex("MProdsub_name")));
                        partsNameList2.add(cursor.getString(cursor.getColumnIndex("MProdsub_name")));

                    } while (cursor.moveToNext());
                    db2.close();
                    cursor.close();
                }
                Log.v("OFF Line", "SQLITE Table");
                return "received";

            }catch (Exception e)
            {
                Log.v("Error in get date","catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }

        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            Set<String> hs = new HashSet<>();
            hs.addAll(partsNameList2);
            partsNameList2.clear();
            partsNameList2.addAll(hs);
            autoCompleteAdapterOthers = new AutoCompleteAdapterOthers(Search_Activity.this, R.layout.simple_spinner_item2, android.R.id.text1, partsNameList2);
           /*autoCompleteTextView.setThreshold(1);
            autoCompleteTextView.setAdapter(partnameadapter);*/
        }

    }

    public class GetPartsForAutotext extends AsyncTask<String, Void, String>
    {
        SQLiteDatabase db2;
        private ArrayList<String> partsNameList = new ArrayList<String>();;
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            try
            {

                DBHelper dbHelper = new DBHelper(Search_Activity.this);
                db2 = dbHelper.readDataBase();

                String query ="select distinct Prod_Name from Product_details";
                Log.v("productname for autotex",query);
                Cursor cursor = db2.rawQuery(query, null);
                if(cursor.moveToFirst())
                {
                    do
                    {
                        partsNameList.add(cursor.getString(cursor.getColumnIndex("Prod_Name")));

                    } while (cursor.moveToNext());
                    db2.close();
                    cursor.close();
                }
                Log.v("OFF Line", "SQLITE Table");
                return "received";

            }catch (Exception e)
            {
                Log.v("Error in get date","catch is working");
                Log.v("Error in Incentive", e.getMessage());
                return "notsuccess";
            }

        }
        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            partnameadapter = new AutoCompleteAdapter(Search_Activity.this, R.layout.simple_spinner_item2, android.R.id.text1, partsNameList);
           /*autoCompleteTextView.setThreshold(1);
            autoCompleteTextView.setAdapter(partnameadapter);*/
        }

    }

    private void CheckInternet() {
        if (network.CheckInternet()) {
            LoginAlert alert= new LoginAlert(Search_Activity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {

            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }

    void performSearch()
    {
        Searchstring=search_text.getText().toString();
        searchtype = Searchspinner.getSelectedItem().toString();
        if(searchtype.equals("Select type"))
        {
            Toast.makeText(getApplicationContext(),"Select Search type",Toast.LENGTH_SHORT).show();
            //search_text.setError("Cannot be empty");
        }else if(Searchstring.equals(""))
        {
            search_text.setError("Cannot be empty");
        }
        else
        {
            Intent i= new Intent(Search_Activity.this,SearchList_Activity.class);
            i.putExtra("Searchtype",searchtype);
            i.putExtra("Searchcontent",Searchstring);
            startActivity(i);
        }
    }
    }

