package api.models.master;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
@Entity(tableName = "Customer")
public class Customer_list{

  @ColumnInfo(name = "Id")
  @PrimaryKey(autoGenerate = true)
  private int Id;

  @ColumnInfo(name = "SalesPersonName")
  private String SalesPersonName;
  @ColumnInfo(name = "Name")
  private String Name;
  @ColumnInfo(name = "BranchName")
  private String BranchName;
  @ColumnInfo(name = "UpdateDateTime")
  private String UpdateDateTime;
  @ColumnInfo(name = "EmailId")
  private String EmailId;
  @ColumnInfo(name = "EmailId1")
  private String EmailId1;
  @ColumnInfo(name = "deleteflag")
  private String deleteflag;
  @ColumnInfo(name = "address2")
  private String address2;
  @ColumnInfo(name = "CustomerCode")
  private String CustomerCode;
  @ColumnInfo(name = "address1")
  private String address1;
  @ColumnInfo(name = "CreditLimit")
  private String CreditLimit;
  @ColumnInfo(name = "LocationCode")
  private String LocationCode;
  @ColumnInfo(name = "Typeofcustomer")
  private String Typeofcustomer;
  @ColumnInfo(name = "City")
  private String City;
  @ColumnInfo(name = "CustomerPostingGroup")
  private String CustomerPostingGroup;
  @ColumnInfo(name = "Mobile")
  private String Mobile;
  @ColumnInfo(name = "deletedate")
  private String deletedate;
  @ColumnInfo(name = "Date")
  private String Date;
  @ColumnInfo(name = "LandLine")
  private String LandLine;
  @ColumnInfo(name = "Mobile1")
  private String Mobile1;
  @ColumnInfo(name = "ContactPerson2")
  private String ContactPerson2;
  @ColumnInfo(name = "ContactPerson1")
  private String ContactPerson1;
  @ColumnInfo(name = "CreditDays")
  private String CreditDays;
  @ColumnInfo(name = "Country")
  private String Country;
  @ColumnInfo(name = "updatetime")
  private String updatetime;
  @ColumnInfo(name = "Pincode")
  private String Pincode;
  @ColumnInfo(name = "BranchCode")
  private String BranchCode;
  public void setSalesPersonName(String SalesPersonName){
   this.SalesPersonName=SalesPersonName;
  }
  public String getSalesPersonName(){
   return SalesPersonName;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
  public void setBranchName(String BranchName){
   this.BranchName=BranchName;
  }
  public String getBranchName(){
   return BranchName;
  }
  public void setUpdateDateTime(String UpdateDateTime){
   this.UpdateDateTime=UpdateDateTime;
  }
  public String getUpdateDateTime(){
   return UpdateDateTime;
  }
  public void setEmailId(String EmailId){
   this.EmailId=EmailId;
  }
  public String getEmailId(){
   return EmailId;
  }
  public void setEmailId1(String EmailId1){
   this.EmailId1=EmailId1;
  }
  public String getEmailId1(){
   return EmailId1;
  }
  public void setDeleteflag(String deleteflag){
   this.deleteflag=deleteflag;
  }
  public String getDeleteflag(){
   return deleteflag;
  }
  public void setAddress2(String address2){
   this.address2=address2;
  }
  public String getAddress2(){
   return address2;
  }
  public void setCustomerCode(String CustomerCode){
   this.CustomerCode=CustomerCode;
  }
  public String getCustomerCode(){
   return CustomerCode;
  }
  public void setAddress1(String address1){
   this.address1=address1;
  }
  public String getAddress1(){
   return address1;
  }
  public void setCreditLimit(String CreditLimit){
   this.CreditLimit=CreditLimit;
  }
  public String getCreditLimit(){
   return CreditLimit;
  }
  public void setLocationCode(String LocationCode){
   this.LocationCode=LocationCode;
  }
  public String getLocationCode(){
   return LocationCode;
  }
  public void setTypeofcustomer(String Typeofcustomer){
   this.Typeofcustomer=Typeofcustomer;
  }
  public String getTypeofcustomer(){
   return Typeofcustomer;
  }
  public void setCity(String City){
   this.City=City;
  }
  public String getCity(){
   return City;
  }
  public void setCustomerPostingGroup(String CustomerPostingGroup){
   this.CustomerPostingGroup=CustomerPostingGroup;
  }
  public String getCustomerPostingGroup(){
   return CustomerPostingGroup;
  }
  public void setMobile(String Mobile){
   this.Mobile=Mobile;
  }
  public String getMobile(){
   return Mobile;
  }
  public void setDeletedate(String deletedate){
   this.deletedate=deletedate;
  }
  public String getDeletedate(){
   return deletedate;
  }
  public void setDate(String Date){
   this.Date=Date;
  }
  public String getDate(){
   return Date;
  }
  public void setLandLine(String LandLine){
   this.LandLine=LandLine;
  }
  public String getLandLine(){
   return LandLine;
  }
  public void setMobile1(String Mobile1){
   this.Mobile1=Mobile1;
  }
  public String getMobile1(){
   return Mobile1;
  }
  public void setContactPerson2(String ContactPerson2){
   this.ContactPerson2=ContactPerson2;
  }
  public String getContactPerson2(){
   return ContactPerson2;
  }
  public void setContactPerson1(String ContactPerson1){
   this.ContactPerson1=ContactPerson1;
  }
  public String getContactPerson1(){
   return ContactPerson1;
  }
  public void setCreditDays(String CreditDays){
   this.CreditDays=CreditDays;
  }
  public String getCreditDays(){
   return CreditDays;
  }
  public void setCountry(String Country){
   this.Country=Country;
  }
  public String getCountry(){
   return Country;
  }
  public void setId(int Id){
   this.Id=Id;
  }
  public int getId(){
   return Id;
  }
  public void setUpdatetime(String updatetime){
   this.updatetime=updatetime;
  }
  public String getUpdatetime(){
   return updatetime;
  }
  public void setPincode(String Pincode){
   this.Pincode=Pincode;
  }
  public String getPincode(){
   return Pincode;
  }
  public void setBranchCode(String BranchCode){
   this.BranchCode=BranchCode;
  }
  public String getBranchCode(){
   return BranchCode;
  }
}