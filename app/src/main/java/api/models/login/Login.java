package api.models.login;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Login{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private LoginData data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(LoginData data){
   this.data=data;
  }
  public LoginData getData(){
   return data;
  }
}