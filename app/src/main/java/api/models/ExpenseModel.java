package api.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ExpenseModel{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<DataItem> data;

	public String getResult(){
		return result;
	}

	public List<DataItem> getData(){
		return data;
	}
}