package api.models.payment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Payment {
  @SerializedName("BankName")
  @Expose
  private String BankName;
  @SerializedName("BillNo")
  @Expose
  private String BillNo;
  @SerializedName("SalesPersonName")
  @Expose
  private String SalesPersonName;
  @SerializedName("Remarks")
  @Expose
  private String Remarks;
  @SerializedName("Amount")
  @Expose
  private Integer Amount;
  @SerializedName("ChqueNumber")
  @Expose
  private String ChqueNumber;
  @SerializedName("PaymentType")
  @Expose
  private String PaymentType;
  @SerializedName("PartyName")
  @Expose
  private String PartyName;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("ChequeDate")
  @Expose
  private String ChequeDate;
  @SerializedName("DueDate")
  @Expose
  private String DueDate;
  @SerializedName("PaidAmount")
  @Expose
  private Integer PaidAmount;
  public void setBankName(String BankName){
   this.BankName=BankName;
  }
  public String getBankName(){
   return BankName;
  }
  public void setBillNo(String BillNo){
   this.BillNo=BillNo;
  }
  public String getBillNo(){
   return BillNo;
  }
  public void setSalesPersonName(String SalesPersonName){
   this.SalesPersonName=SalesPersonName;
  }
  public String getSalesPersonName(){
   return SalesPersonName;
  }
  public void setRemarks(String Remarks){
   this.Remarks=Remarks;
  }
  public String getRemarks(){
   return Remarks;
  }
  public void setAmount(Integer Amount){
   this.Amount=Amount;
  }
  public Integer getAmount(){
   return Amount;
  }
  public void setChqueNumber(String ChqueNumber){
   this.ChqueNumber=ChqueNumber;
  }
  public String getChqueNumber(){
   return ChqueNumber;
  }
  public void setPaymentType(String PaymentType){
   this.PaymentType=PaymentType;
  }
  public String getPaymentType(){
   return PaymentType;
  }
  public void setPartyName(String PartyName){
   this.PartyName=PartyName;
  }
  public String getPartyName(){
   return PartyName;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setChequeDate(String ChequeDate){
   this.ChequeDate=ChequeDate;
  }
  public String getChequeDate(){
   return ChequeDate;
  }
  public void setDueDate(String DueDate){
   this.DueDate=DueDate;
  }
  public String getDueDate(){
   return DueDate;
  }
  public void setPaidAmount(Integer PaidAmount){
   this.PaidAmount=PaidAmount;
  }
  public Integer getPaidAmount(){
   return PaidAmount;
  }
}