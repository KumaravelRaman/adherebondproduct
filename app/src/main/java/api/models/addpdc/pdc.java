package api.models.addpdc;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class pdc {
  @SerializedName("BankName")
  @Expose
  private Object BankName;
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("SalesPerson")
  @Expose
  private String SalesPerson;
  @SerializedName("Amount")
  @Expose
  private Object Amount;
  @SerializedName("Customername")
  @Expose
  private String Customername;
  @SerializedName("Branchname")
  @Expose
  private Object Branchname;
  @SerializedName("CustomerId")
  @Expose
  private Object CustomerId;
  @SerializedName("DeleteDate")
  @Expose
  private Object DeleteDate;
  @SerializedName("Branchcode")
  @Expose
  private Object Branchcode;
  @SerializedName("Deleteflag")
  @Expose
  private String Deleteflag;
  @SerializedName("UpdateDate")
  @Expose
  private Object UpdateDate;
  @SerializedName("Acc_No")
  @Expose
  private Object Acc_No;
  @SerializedName("Customercode")
  @Expose
  private Object Customercode;
  @SerializedName("Pdc_ID")
  @Expose
  private Integer Pdc_ID;
  @SerializedName("ChequeDate")
  @Expose
  private Object ChequeDate;
  @SerializedName("ChequeNo")
  @Expose
  private Object ChequeNo;
  @SerializedName("CurrentDate")
  @Expose
  private String CurrentDate;
  @SerializedName("Bankaddress")
  @Expose
  private Object Bankaddress;
  public void setBankName(Object BankName){
   this.BankName=BankName;
  }
  public Object getBankName(){
   return BankName;
  }
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setSalesPerson(String SalesPerson){
   this.SalesPerson=SalesPerson;
  }
  public String getSalesPerson(){
   return SalesPerson;
  }
  public void setAmount(Object Amount){
   this.Amount=Amount;
  }
  public Object getAmount(){
   return Amount;
  }
  public void setCustomername(String Customername){
   this.Customername=Customername;
  }
  public String getCustomername(){
   return Customername;
  }
  public void setBranchname(Object Branchname){
   this.Branchname=Branchname;
  }
  public Object getBranchname(){
   return Branchname;
  }
  public void setCustomerId(Object CustomerId){
   this.CustomerId=CustomerId;
  }
  public Object getCustomerId(){
   return CustomerId;
  }
  public void setDeleteDate(Object DeleteDate){
   this.DeleteDate=DeleteDate;
  }
  public Object getDeleteDate(){
   return DeleteDate;
  }
  public void setBranchcode(Object Branchcode){
   this.Branchcode=Branchcode;
  }
  public Object getBranchcode(){
   return Branchcode;
  }
  public void setDeleteflag(String Deleteflag){
   this.Deleteflag=Deleteflag;
  }
  public String getDeleteflag(){
   return Deleteflag;
  }
  public void setUpdateDate(Object UpdateDate){
   this.UpdateDate=UpdateDate;
  }
  public Object getUpdateDate(){
   return UpdateDate;
  }
  public void setAcc_No(Object Acc_No){
   this.Acc_No=Acc_No;
  }
  public Object getAcc_No(){
   return Acc_No;
  }
  public void setCustomercode(Object Customercode){
   this.Customercode=Customercode;
  }
  public Object getCustomercode(){
   return Customercode;
  }
  public void setPdc_ID(Integer Pdc_ID){
   this.Pdc_ID=Pdc_ID;
  }
  public Integer getPdc_ID(){
   return Pdc_ID;
  }
  public void setChequeDate(Object ChequeDate){
   this.ChequeDate=ChequeDate;
  }
  public Object getChequeDate(){
   return ChequeDate;
  }
  public void setChequeNo(Object ChequeNo){
   this.ChequeNo=ChequeNo;
  }
  public Object getChequeNo(){
   return ChequeNo;
  }
  public void setCurrentDate(String CurrentDate){
   this.CurrentDate=CurrentDate;
  }
  public String getCurrentDate(){
   return CurrentDate;
  }
  public void setBankaddress(Object Bankaddress){
   this.Bankaddress=Bankaddress;
  }
  public Object getBankaddress(){
   return Bankaddress;
  }
}