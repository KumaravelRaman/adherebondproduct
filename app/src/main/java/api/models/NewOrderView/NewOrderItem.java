package api.models.NewOrderView;

import com.google.gson.annotations.SerializedName;

public class NewOrderItem {

	@SerializedName("customertype")
	private String customertype;

	@SerializedName("unpackage")
	private String unpackage;

	@SerializedName("orderType")
	private String orderType;

	@SerializedName("package")
	private String jsonMemberPackage;

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("color")
	private String color;

	@SerializedName("OrderMode")
	private String orderMode;

	@SerializedName("discount")
	private String discount;

	@SerializedName("mobileno")
	private String mobileno;

	@SerializedName("basicprice")
	private String basicprice;

	@SerializedName("number")
	private String number;

	@SerializedName("totalamount")
	private String totalamount;

	@SerializedName("productname")
	private String productname;

	@SerializedName("insertdate")
	private String insertdate;

	@SerializedName("Id")
	private int id;

	@SerializedName("customername")
	private String customername;

	@SerializedName("remarks")
	private String remarks;

	public String getCustomertype(){
		return customertype;
	}

	public String getUnpackage(){
		return unpackage;
	}

	public String getOrderType(){
		return orderType;
	}

	public String getJsonMemberPackage(){
		return jsonMemberPackage;
	}

	public String getQuantity(){
		return quantity;
	}

	public String getColor(){
		return color;
	}

	public String getOrderMode(){
		return orderMode;
	}

	public String getDiscount(){
		return discount;
	}

	public String getMobileno(){
		return mobileno;
	}

	public String getBasicprice(){
		return basicprice;
	}

	public String getNumber(){
		return number;
	}

	public String getTotalamount(){
		return totalamount;
	}

	public String getProductname(){
		return productname;
	}

	public String getInsertdate(){
		return insertdate;
	}

	public int getId(){
		return id;
	}

	public String getCustomername(){
		return customername;
	}

	public String getRemarks(){
		return remarks;
	}
}