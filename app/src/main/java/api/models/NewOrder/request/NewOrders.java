
package api.models.NewOrder.request;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class NewOrders {

    @SerializedName("New_Orders_List")
    private List<NewOrdersList> mNewOrdersList;

    public List<NewOrdersList> getNewOrdersList() {
        return mNewOrdersList;
    }

    public void setNewOrdersList(List<NewOrdersList> newOrdersList) {
        mNewOrdersList = newOrdersList;
    }

}
