package SQL_Fields;

/**
 * Created by SYSTEM02 on 11/30/2017.
 */

public class SQLITE {
    public  static final String DATABASE_NAME = "CERADATA1";
    public static final String TABLE_COUSTOMER = "Customer";
    public static final String COLUMN_id = "id";
    public static final String COLUMN_CustomerCode = "CustomerCode";
    public static final String COLUMN_Name = "Name";
    public static final String COLUMN_Mobile = "Mobile";
    public static final String COLUMN_EmailId = "EmailId";
    public static final String COLUMN_LandLine = "LandLine";
    public static final String COLUMN_address1 = "address1";
    public static final String COLUMN_address2 = "address2";
    public static final String COLUMN_City = "City";
    public static final String COLUMN_Pincode = "Pincode";
    public static final String COLUMN_Country = "Country";
    public static final String COLUMN_ContactPerson1 = "ContactPerson1";
    public static final String COLUMN_ContactPerson2 = "ContactPerson2";
    public static final String COLUMN_BranchCode = "BranchCode";
    public static final String COLUMN_CreditDays = "CreditDays";
    public static final String COLUMN_CreditLimit = "CreditLimit";
    public static final String COLUMN_SalesPersonName = "SalesPersonName";
    public static final String COLUMN_UpdateDateTime = "UpdateDateTime";
    public static final String COLUMN_Date = "Date";
    public static final String COLUMN_deleteflag = "deleteflag";
    public static final String COLUMN_updatetime = "updatetime";
    public static final String COLUMN_deletedate = "deletedate";

    //AgingBalance
    public static final String TABLE_AGE = "VMAgeing";
    public static final String COLUMN_AGE_id = "id";
    public static final String COLUMN_AGE_CustomerCode = "CustomerCode";
    public static final String COLUMN_AGE_CustomerName = "CustomerName";
    public static final String COLUMN_AGE_DocumentNumber = "DocumentNumber";
    public static final String COLUMN_AGE_DocumentType = "DocumentType";
    public static final String COLUMN_AGE_Date = "Date";
    public static final String COLUMN_AGE_Amount = "Amount";
    public static final String COLUMN_AGE_CreditDays = "CreditDays";
    public static final String COLUMN_AGE_ExcessDays = "ExcessDays";
    public static final String COLUMN_AGE_Days = "Days";
    public static final String COLUMN_AGE_Mobile = "Mobile";
    public static final String COLUMN_AGE_EmailId = "EmailId";
    public static final String COLUMN_AGE_LandLine = "LandLine";
    public static final String COLUMN_AGE_address1 = "address1";
    public static final String COLUMN_AGE_address2 = "address2";
    public static final String COLUMN_AGE_City = "City";
    public static final String COLUMN_AGE_Pincode = "Pincode";
    public static final String COLUMN_AGE_Country = "Country";
    public static final String COLUMN_AGE_ContactPerson1 = "ContactPerson1";
    public static final String COLUMN_AGE_ContactPerson2 = "ContactPerson2";
    public static final String COLUMN_AGE_BranchCode = "BranchCode";
    public static final String COLUMN_AGE_CreditLimit = "CreditLimit";
    public static final String COLUMN_AGE_Expr2 = "Expr2";
    public static final String COLUMN_AGE_SalesPersonName = "SalesPersonName";


    //PDC
    public static final String TABLE_PDC = "VWPdccheque";
    public static final String COLUMN_PDC_ID = "Pdc_ID";
    public static final String COLUMN_PDC_Dealername = "Dealername";
    public static final String COLUMN_PDC_Dealecode = "Dealecode";
    public static final String COLUMN_PDC_DealerId = "DealerId";
    public static final String COLUMN_PDC_Mobile = "Mobile";
    public static final String COLUMN_PDC_EmailId = "EmailId";
    public static final String COLUMN_PDC_LandLine = "LandLine";
    public static final String COLUMN_PDC_City = "City";
    public static final String COLUMN_PDC_CreditLimit = "CreditLimit";
    public static final String COLUMN_PDC_CreditDays = "CreditDays";
    public static final String COLUMN_PDC_ChequeNo = "ChequeNo";
    public static final String COLUMN_PDC_ChequeDate = "ChequeDate";
    public static final String COLUMN_PDC_CurrentDate = "CurrentDate";
    public static final String COLUMN_PDC_UpdateDate = "UpdateDate";
    public static final String COLUMN_PDC_DeleteDate = "DeleteDate";
    public static final String COLUMN_PDC_Deleteflag = "Deleteflag";
    public static final String COLUMN_PDC_BankName = "BankName";
    public static final String COLUMN_PDC_Branchname = "Branchname";
    public static final String COLUMN_PDC_Bankaddress = "Bankaddress";
    public static final String COLUMN_PDC_Branchcode = "BranchCode";
    public static final String COLUMN_PDC_Acc_No = "Acc_No";
    public static final String COLUMN_PDC_Amount = "Amount";
    public static final String COLUMN_PDC_Status = "Status";
    public static final String COLUMN_PDC_SalesPerson = "SalesPersonName";


    //TrialBalance
    public static final String TABLE_TrialBalance = "VWCustomerBalance";
    public static final String COLUMN_T_id = "id";
    public static final String COLUMN_T_CustomerCode = "CustomerCode";
    public static final String COLUMN_T_CustomerName = "CustomerName";
    public static final String COLUMN_T_CustomerMobile = "Mobile";
    public static final String COLUMN_T_CustomerEmailId = "EmailId";
    public static final String COLUMN_T_CustomerTrailBalance = "TrailBalance";
    public static final String COLUMN_T_CustomerDateOfImport = "DateOfImport";
    public static final String COLUMN_T_CustomerBranchCode = "BranchCode";
    public static final String COLUMN_T_CustomerLandLine = "LandLine";
    public static final String COLUMN_T_Customeraddress1 = "address1";
    public static final String COLUMN_T_Customeraddress2 = "address2";
    public static final String COLUMN_T_CustomerCity = "City";
    public static final String COLUMN_T_CustomerPincode = "Pincode";
    public static final String COLUMN_T_CustomerCountry = "Country";
    public static final String COLUMN_T_CustomerCreditDays = "CreditDays";
    public static final String COLUMN_T_CustomerCreditLimit = "CreditLimit";
    public static final String COLUMN_T_CustomerSalesPersonName = "SalesPersonName";
    public static final String COLUMN_T_CustomerBalanceAsOn = "BalanceAsOn";

    // Table Order

    public static final String TABLE_PartsOrder = "PartsOrder";
    public static final String COLUMN_executive = "SalesName";
    public static final String COLUMN_customername = "CustomerName";
    public static final String COLUMN_orderID = "orderNo";
    public static final String COLUMN_productname = "ProductName";
    public static final String COLUMN_package = "Package";
    public static final String COLUMN_partno = "PartNO";
    public static final String COLUMN_discount = "discount";
    public static final String COLUMN_quantity = "quantity";
    public static final String COLUMN_unitprice = "unit";
    public static final String COLUMN_amount = "amount";
    public static final String COLUMN_currentdate="formattedDate";
    public static final String COLUMN_OrderType="OrderType";
    public static final String COLUMN_cutomermobile = "MobileNO";
    public static final String COLUMN_customeremail = "string_mail";

// Table Create Profile

    public static final String TABLE_Create_Profile = "Createprofile";
    public static final String COLUMN_Create_cusname = "cusname";
    public static final String COLUMN_Create_address = "address";
    public static final String COLUMN_Create_city = "city";
    public static final String COLUMN_Create_state = "state";
    public static final String COLUMN_Create_pincode = "pincode";
    public static final String COLUMN_Create_contact = "contact";
    public static final String COLUMN_Create_contactname = "contactname";
    public static final String COLUMN_Create_email = "email";
    public static final String COLUMN_Create_gst = "gst";
    public static final String COLUMN_Create_pan= "pan";
    public static final String COLUMN_Create_deliveryaddress= "deliveryaddress";
    public static final String COLUMN_Create_custype = "custype";

    // Table NewOrder

    public static final String TABLE_NewPartsOrder = "NewOrderpreview";
    public static final String COLUMN_Newid = "id";
    public static final String COLUMN_executivename = "executivename";
    public static final String COLUMN_dealername = "dealername";
    public static final String COLUMN_address = "address";
    public static final String COLUMN_deliveryaddress= "deliveryaddress";
    public static final String COLUMN_city = "city";
    public static final String COLUMN_state = "state";
    public static final String COLUMN_pincode = "pincode";
    public static final String COLUMN_contact = "contact";
    public static final String COLUMN_contactname = "contactname";
    public static final String COLUMN_email = "email";
    public static final String COLUMN_gst = "gst";
    public static final String COLUMN_pan= "pan";
    public static final String COLUMN_custype = "custype";
    public static final String COLUMN_prodname = "productname";
    public static final String COLUMN_pack = "package";
    public static final String COLUMN_quant = "quantity";
    public static final String COLUMN_discounts = "discount";
    public static final String COLUMN_orderType = "orderType";
    public static final String COLUMN_date = "date";
    public static final String COLUMN_remark = "remark";
    public static final String COLUMN_ordermode = "orderMode";
    public static final String COLUMN_ordernumber = "numbers";
    public static final String COLUMN_ordercolours = "colours";
    public static final String Coloumn_umpackage = "Unpackage";


    // Table Payments

    public static final String TABLE_payment_details = "payment_details";
    public static final String COLUMN_salesperson_name = "salesperson_name";
    public static final String COLUMN_dealer_name = "dealer_name";
    public static final String COLUMN_payment_type = "payment_type";
    public static final String COLUMN_Aamount = "amount";
    public static final String COLUMN_bank_name = "bank_name";
    public static final String COLUMN_cheque_no = "cheque_no";
    public static final String COLUMN_dd_number = "dd_number";
    public static final String COLUMN_credit_number = "credit_number";
    public static final String COLUMN_Ddate = "date";
    public static final String COLUMN_Ccutomermobile = "cutomermobile";
    public static final String COLUMN_string_mail = "string_mail";



    // Table Additems

    public static final String TABLE_Additems = "Orderpreview";
    public static final String COLUMN_Addid = "id";
    public static final String COLUMN_Addexecutive = "SalesName";
    public static final String COLUMN_Addcustomername = "CustomerName";
    public static final String COLUMN_Addproductname = "ProductName";
    public static final String COLUMN_Addpackage = "Package";
    public static final String COLUMN_Addquantity = "quantity";
    public static final String COLUMN_Adddiscount = "discount";
    public static final String COLUMN_AddOrderType="OrderType";
    public static final String COLUMN_Addcurrentdate="formattedDate";
    public static final String COLUMN_Addcustomercode="Customercode";
    public static final String COLUMN_AddMobileNO="MobileNO";
    public static final String COLUMN_Addstring_mail="string_mail";
    public static final String COLUMN_AddBranchCode="BranchCode";
    public static final String COLUMN_AddRemarks="Remarks";
    public static final String COLUMN_Addstring_ordermode="order_mode";
    public static final String COLUMN_Addstring_ordernumber="numbers";
    public static final String COLUMN_Addstring_ordercolors="colours";
    public static final String COLUMN_Unpackage="Unpackage";

    public static final String COLUMN_basic="basicprice";
    public static final String COLUMN_discountprice="discountprice";
    public static final String COLUMN_total="totalprice";

    public static final String TABLE_coorinates = "coordinates";
    public static final String COLUMN_Coo_id = "id";
    public static final String COLUMN_Coo_S_id = "S_id";
    public static final String COLUMN_Coo_secure_id = "secure_id";
    public static final String COLUMN_Coo_saname = "saname";
    public static final String COLUMN_Coo_latitude = "latitude";
    public static final String COLUMN_Coo_langtitude = "langtitude";
    public static final String COLUMN_Coo_cityname = "cityname";
    public static final String COLUMN_Coo_description = "description";
    public static final String COLUMN_Coo_datetime = "datetime";
    public static final String COLUMN_Coo_timedate = "timedate";


}
