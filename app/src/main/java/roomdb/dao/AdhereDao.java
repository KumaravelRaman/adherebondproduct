package roomdb.dao;



import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import api.models.master.Amt_list;
import api.models.master.Customer_list;
import api.models.master.PAl_list;


@Dao
public interface AdhereDao {

    // Deteting data
    @Query("DELETE FROM Customer")
    void DeleteAllCustomer();

    @Query("DELETE FROM VMAgeing")
    void DeleteAllAgeing();

    @Query("DELETE FROM VWCustomerBalance")
    void DeleteAllPayments();

    // Inserting data
    @Insert
    void insertAllCustomer(List<Customer_list> customer);

    @Insert
    void insertAllAgeing(List<Amt_list> amt_lists);

    @Insert
    void insertAllPaymets(List<PAl_list> pAl_lists);


    // Getting data
    @Query("SELECT * FROM Customer")
    List<Customer_list> GetCustomer_list();


    @Query("SELECT * FROM VWCustomerBalance")
    List<PAl_list> GetPailist();
    // Getting data
    @Query("SELECT * FROM Customer where Name = :customername")
    List<Customer_list> GetCustomer_list_Name(String customername);

    @Query("SELECT * FROM VWCustomerBalance where PartyName = :customername")
    List<PAl_list> GetPailist(String customername);

    // Getting data
    @Query("SELECT Name  FROM Customer")
    List<String> GetCustomer_name_list();


}
