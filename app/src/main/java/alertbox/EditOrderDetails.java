package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.brainmagic.adherebond.R;

//import com.brainmagic.adherebond.R;


public class EditOrderDetails {
	Context context;

	public EditOrderDetails(Context con) {
		// TODO Auto-generated constructor stub
		this.context = con;
	}
	

	public void showAlertbox(String name, String product, String pack, String type, String discount, String quantity)
	{
		final AlertDialog alertDialog = new Builder(
				context).create();

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.update_order, null);
		alertDialog.setView(dialogView);

		TextView Name = (TextView) dialogView.findViewById(R.id.customer);
		TextView Product = (TextView) dialogView.findViewById(R.id.product);
		TextView Pack = (TextView) dialogView.findViewById(R.id.pack);
		TextView Type = (TextView) dialogView.findViewById(R.id.type);
		EditText Discount = (EditText) dialogView.findViewById(R.id.discount);
		EditText Quantity = (EditText) dialogView.findViewById(R.id.quantity);

		Button Save = (Button)  dialogView.findViewById(R.id.save);
		Button Cancel = (Button)  dialogView.findViewById(R.id.cancel);






		Save.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				alertDialog.dismiss();
			}
		});
		alertDialog.show();
		Cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				alertDialog.dismiss();
			}
		});
	}

}
