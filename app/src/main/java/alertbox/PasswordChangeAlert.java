package alertbox;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


//import com.brainmagic.adherebond.R;

import com.brainmagic.adherebond.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import network.NetworkConnection;
import persistency.ServerConnection;

import static android.content.Context.MODE_PRIVATE;


public class PasswordChangeAlert {
	Context context;
	Connection connection;
	Statement statement;
	ResultSet resultSet;
	private ProgressDialog progress;
	private SharedPreferences myshare;
	private SharedPreferences.Editor editor;
	private String SalesPass,SalesId,Old,New,Confirm;

	//editor = myshare.edit();

	public PasswordChangeAlert(Context con) {
		// TODO Auto-generated constructor stub
		this.context = con;
			}

//	editor = myshare.edit();

	public void showLoginbox()
	{

		myshare = context.getSharedPreferences("Registration", MODE_PRIVATE);
		editor = myshare.edit();
		final AlertDialog alertDialog = new Builder(
				context).create();
		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.changepasswordalert, null);
		alertDialog.setView(dialogView);
		//TextView log = (TextView) dialogView.findViewById(R.id.loglabel);

		final EditText Old_password=(EditText)dialogView.findViewById(R.id.old_password);
		final EditText New_password=(EditText)dialogView.findViewById(R.id.new_password);
		final EditText Confirm_password=(EditText)dialogView.findViewById(R.id.confirm_password);
		Button OK=(Button) dialogView.findViewById(R.id.okay2);
		Button Cancel=(Button) dialogView.findViewById(R.id.cancel);
		Cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				alertDialog.dismiss();
			}
		});



		OK.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Old = Old_password.getText().toString();
				New = New_password.getText().toString();
				Confirm = Confirm_password.getText().toString();
				SalesPass =  myshare.getString("pass", "");
                SalesId =  myshare.getString("salesID", "");


				if(Old.equals("")){
					Old_password.setError("Enter current password");
				} else if(New.equals("")){
					New_password.setError("Enter new password");
				} else if (Confirm.equals("")){
					Confirm_password.setError("Enter confirm password");
				} else if(!SalesPass.equalsIgnoreCase(Old)) {
                    Toast.makeText(context,"Current Password is incorrect!", Toast.LENGTH_SHORT).show();
                } else if(!New.equalsIgnoreCase(Confirm)) {
                    Toast.makeText(context,"New password & Confirm password doesn't match !", Toast.LENGTH_SHORT).show();
                }
				else {
				    CheckInternet();
					alertDialog.dismiss();
                }

			}
		});

		alertDialog.show();
	}


	private void CheckInternet() {
		try {
			NetworkConnection network = new NetworkConnection(context);
			if (network.CheckInternet()) {
				new changepassword().execute();
				//askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, WRITE_EXST);
			} else {
				Alertbox alert = new Alertbox(context);
				alert.showAlertbox("Kindly check your Internet Connection");
			}

		}catch (Exception e) {
			e.printStackTrace();

		}
	}

	private class changepassword extends AsyncTask<String, Void, String> {
		int i;
		protected void onPreExecute() {
			super.onPreExecute();
			progress = new ProgressDialog(context);
			progress.setMessage("Loading...");
			progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progress.setCancelable(false);
			progress.show();
		}

		@Override
		protected String doInBackground(String... strings) {

			ServerConnection serverConnection = new ServerConnection();
			try {
				connection = serverConnection.getConnection();
				statement = connection.createStatement();
                String selectquery = "UPDATE CeraLogin SET password = '"+New+"' WHERE id = '"+SalesId+"'";
				Log.v("query", selectquery);
                i=statement.executeUpdate(selectquery);
                if(i==1) {

                    connection.close();
                    statement.close();
                    return "updated";}
                else {
                    connection.close();
                    statement.close();
                    return "notupdated";
                }
			} catch (Exception e) {
				e.printStackTrace();
				return "error_insertion";
			}

		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);
			progress.dismiss();
			if (s.equals("updated")) {
				editor.putString("pass", New);
				editor.commit();
				Alertbox alert = new Alertbox(context);
				alert.showAlertbox("Password changed successfully");
			} else {
				Alertbox alert = new Alertbox(context);
				alert.showAlertbox("Error occurred try again");
			}
		}
	}
	}




