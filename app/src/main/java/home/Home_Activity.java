package home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Login_Activity;
import com.brainmagic.adherebond.PaymentWelcomeActivity;
import com.brainmagic.adherebond.ProjectActivity;
import com.brainmagic.adherebond.Welcome_Activity;
import com.halilibo.bettervideoplayer.subtitle.CaptionsView;

import Expense.ExpenseActivity;
import Logout.logout;
import about.About_Activity;
import alertbox.Alertbox;
import alertbox.LoginAlert;
import alertbox.PasswordChangeAlert;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import product.Product_Activity;
import search.Search_Activity;


public class Home_Activity extends AppCompatActivity {

    private static Context appContext;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private LinearLayout project, enquiry, searchlayout, offer;
    NetworkConnection network = new NetworkConnection(Home_Activity.this);
    Alertbox alert = new Alertbox(Home_Activity.this);
    ImageView menu;
    String usertype = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        myshare = getSharedPreferences("Registration", Home_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();

        appContext = this;
        //   wv.loadData(yourData, "text/html", "UTF-8");
        searchlayout = (LinearLayout) findViewById(R.id.search_layout);
        menu = (ImageView) findViewById(R.id.menubar);
        enquiry = (LinearLayout) findViewById(R.id.enquiry);
        project = (LinearLayout) findViewById(R.id.project);
        offer = (LinearLayout) findViewById(R.id.offer);


        offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent c = new Intent(Home_Activity.this, Offers_Activity.class);
                startActivity(c);
            }
        });

        enquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent c = new Intent(Home_Activity.this, Enquiry_Activity.class);
                startActivity(c);
            }
        });
        searchlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent e = new Intent(Home_Activity.this, Search_Activity.class);
                startActivity(e);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Home_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub


                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Home_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;

                            case R.id.expense:
                                PasswordChangeAlert alert = new PasswordChangeAlert(Home_Activity.this);// new saleslogin().execute();
                                alert.showLoginbox();
                                return true;
                            case R.id.productmenu:
                                Intent search1 = new Intent(Home_Activity.this, Product_Activity.class);
                                startActivity(search1);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Home_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;



                            case R.id.contactmenu:
                                Intent contact = new Intent(Home_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Home_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.watsnew:
                                if (network.CheckInternet()) {
                                    Intent f = new Intent(Home_Activity.this, ProjectActivity.class);
                                    startActivity(f);
                                } else {
                                    Alertbox alerts = new Alertbox(Home_Activity.this);
                                    alerts.showAlertbox("Kindly check your Internet Connection");
                                }
                                return true;
                            case R.id.offers:
                                usertype = myshare.getString("Usertype", "");
                                if (usertype.equals("Cera Employee")) {
                                    if (!myshare.getBoolean("islogin", false)) {
                                        CheckInternet();
                                        Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));
                                        Log.v("is  Login ", Boolean.toString(myshare.getBoolean("alertlogin", false)));
                                    } else {
                                        if (network.CheckInternet()) {
                                            Intent f = new Intent(Home_Activity.this, Offers_Activity.class);
                                            startActivity(f);
                                        } else {

//                                       al.showAlertbox("Kindly check your Internet Connection");
                                        }

                                    }
                                } else {
                                    Intent f = new Intent(Home_Activity.this, Offers_Activity.class);
                                    startActivity(f);
                                }
                                return true;


                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin", false)) {
                                    //menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(Home_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));
                                } else {

                                    Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", false)));
                                    ///menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(Home_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Home_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popupmenu_others);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin", false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();


            }
        });
        project.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent d = new Intent(Home_Activity.this, ProjectActivity.class);
                startActivity(d);
            }
        });

    }


    private void CheckInternet() {
        if (network.CheckInternet()) {
            LoginAlert alert = new LoginAlert(Home_Activity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }

    public static Context getContext() {
        return appContext;
    }

    public void onProducts(View view) {

        Intent d = new Intent(Home_Activity.this, Product_Activity.class);
        d.putExtra("a", "");
        startActivity(d);
    }

    public void onAbout(View view) {
        Intent d = new Intent(Home_Activity.this, About_Activity.class);
        startActivity(d);
    }

    public void onContact(View view) {
        Intent d = new Intent(Home_Activity.this, Contact_Activity.class);
        startActivity(d);
    }

    public void onCalculator(View view) {

       /* Intent d = new Intent(Home_Activity.this, Calculator_Activity.class);
        startActivity(d);*/

        String url = "http://adherebond.brainmagicllc.com/productcalculatormobile.html";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);

    }

    public void onLogin(View view) {
        if ((myshare.getBoolean("islogin", false))) {
            if(myshare.getString("usertype","").equals("SalesExecutive")){
                Intent d = new Intent(Home_Activity.this, Welcome_Activity.class);
                startActivity(d);
            }
            else{
                Intent d = new Intent(Home_Activity.this, PaymentWelcomeActivity.class);
                startActivity(d);
            }
        }
        else {
            Log.v("is  Login ", Boolean.toString(myshare.getBoolean("islogin", true)));
            Intent b = new Intent(Home_Activity.this, Login_Activity.class);
            b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(b);
        }
    }
}
