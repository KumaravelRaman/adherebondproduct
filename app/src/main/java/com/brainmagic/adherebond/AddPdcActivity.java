package com.brainmagic.adherebond;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.adherebond.R;
import com.google.android.material.snackbar.Snackbar;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Logout.logout;
import about.About_Activity;
import alertbox.Alertbox;
import api.models.addpdc.PdcData;
import api.models.addpdc.pdc;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import roomdb.database.AppDatabase;
import search.Search_Activity;

import static roomdb.database.AppDatabase.getAppDatabase;

public class AddPdcActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST = 100;
    private Spinner dealerspinner,bankname;
    private EditText chequeno,Amountedit;
    private  static  EditText chequedate;
    ImageView home,back,menu;
    private Button add;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private List<String> dealerNameList,banknamelist;
    private AppDatabase db;
    private String SalesName,customername;
    private  String Status="Unclear";
    private Alertbox box = new Alertbox(AddPdcActivity.this);

    private String banknamelist1;
    private pdc Data;
    private static  String fromstring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pdc);
        dealerspinner=(Spinner)findViewById(R.id.dealerspinner);
        bankname=(Spinner)findViewById(R.id.bankname);
        chequeno=(EditText)findViewById(R.id.chequeno);
        chequedate=(EditText)findViewById(R.id.chequedate);
        Amountedit=(EditText)findViewById(R.id.Amountedit);
        add=(Button)findViewById(R.id.add);
        menu=(ImageView)findViewById(R.id.menubar);
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        db = getAppDatabase(AddPdcActivity.this);
        dealerNameList=new ArrayList<>();
        banknamelist=new ArrayList<>();
        myshare = getSharedPreferences("Registration", AddPdcActivity.this.MODE_PRIVATE);
        editor = myshare.edit();
        SalesName = myshare.getString("name", "");

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(AddPdcActivity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        bankname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                banknamelist1=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        chequedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new FromDatePickerFragment2();
                newFragment.show(getSupportFragmentManager(),"datePicker");
            }
        });
        dealerspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                customername=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        banknamelist = new ArrayList<>();
        banknamelist.add("Select Bank Name");
        banknamelist.add("Karur vysya Bank");
        banknamelist.add("Allahabad bank");
        banknamelist.add("Indian Bank");
        banknamelist.add("Tamilnadu Mercantile bank");
        banknamelist.add("South Indian Bank");
        banknamelist.add("City Union Bank ");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(AddPdcActivity.this, R.layout.simple_spinner_item, banknamelist);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        bankname.setAdapter(spinnerAdapter);
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(AddPdcActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(AddPdcActivity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(AddPdcActivity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(AddPdcActivity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(AddPdcActivity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(AddPdcActivity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(AddPdcActivity.this, com.brainmagic.adherebond.Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(AddPdcActivity.this, com.brainmagic.adherebond.Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(AddPdcActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        Getpermissionforsms();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dealerspinner.equals("")){
                    Toast.makeText(getApplicationContext(), "Select Customer Name", Toast.LENGTH_LONG).show();
                } else if(chequeno.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Select Chequeno", Toast.LENGTH_LONG).show();


                }else if(chequedate.getText().toString().equals("")){

                    Toast.makeText(getApplicationContext(), "Select Cheque date", Toast.LENGTH_LONG).show();

                }else if(Amountedit.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Select Amount", Toast.LENGTH_LONG).show();


                }else if(bankname.equals("")){

                    Toast.makeText(getApplicationContext(), "Select Bank Name", Toast.LENGTH_LONG).show();

                }else {
                    checkinternet();
                }

            }
        });



    }

    private void checkinternet() {
        NetworkConnection isnet = new NetworkConnection(AddPdcActivity.this);
        if (isnet.CheckInternet()) {
            GetServiceEngineerComplaints();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }
    public static class FromDatePickerFragment2 extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        DecimalFormat mFormat= new DecimalFormat("00");


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            chequedate.setText(mFormat.format(day) + "/" + mFormat.format(month + 1) + "/" + year);
            //visitdate.setText(String.format("%d/%d/%d",mFormat.format(day) ,(month + 1) , year));
            fromstring = year + "-" + (month + 1) + "-" + day;
            chequedate.clearFocus();

        }


    }

    private void GetServiceEngineerComplaints() {
        try {
            final ProgressDialog loading = ProgressDialog.show(AddPdcActivity.this, "Adhere bonds", "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<PdcData> call = service.pdc(customername,
                    chequeno.getText().toString(),
                    chequedate.getText().toString(),
                    banknamelist1,
                    Amountedit.getText().toString(),
                    SalesName);
            call.enqueue(new Callback<PdcData>() {
                @Override
                public void onResponse(Call<PdcData> call, Response<PdcData> response) {
                    try {
                        loading.dismiss();

                        if (response.body().getResult().equals("Success")) {
                            Data = response.body().getData();
                            SuccessRegister(response.body().getData());


                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }else if(response.body().getResult().equals("NoData")){
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.v("GetServiceComplaints", e.getMessage());
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<PdcData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void SuccessRegister(pdc data) {
        final AlertDialog alertDialogbox = new AlertDialog.Builder(
                AddPdcActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialogbox.setView(dialogView);

        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText("Pdc Added Successfully");
        okay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent go = new Intent(AddPdcActivity.this, com.brainmagic.adherebond.Payments_Activity.class);
                go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(go);
                alertDialogbox.dismiss();
                finish();
            }
        });
        alertDialogbox.show();
    }


    private void Getpermissionforsms() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CALL_PHONE)) {
                    Snackbar.make(findViewById(R.id.activity_view_cusomers), "You need to give permission", Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onClick(View v) {
                            requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                        }
                    }).show();
                } else {
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_REQUEST);
                }
            } else {
                GetDealerNamesForSpinner1();
            }
        } else
        {
            GetDealerNamesForSpinner1();

        }
    }

    private void GetDealerNamesForSpinner1() {
        dealerNameList =  db.adhereDao().GetCustomer_name_list();
        dealerNameList.add(0,"Select Customer name");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(AddPdcActivity.this,R.layout.simple_spinner_item,dealerNameList);
        spinnerAdapter.setNotifyOnChange(true);
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        dealerspinner.setAdapter(spinnerAdapter);
    }

}

