package com.brainmagic.adherebond;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;

import com.brainmagic.adherebond.R;

import Logout.logout;
import about.About_Activity;
import alertbox.Alertbox;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import search.Search_Activity;

public class PaymentsActivty2 extends AppCompatActivity {
    ImageView home, back, menu;
    RelativeLayout Outstanding_layout,Payment_Layout,pdccheque;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    NetworkConnection network = new NetworkConnection(PaymentsActivty2.this);
    Alertbox alert = new Alertbox(PaymentsActivty2.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments_activty2);
        Payment_Layout = (RelativeLayout)findViewById(R.id.paymentlayout);
        myshare = getSharedPreferences("Registration", Context.MODE_PRIVATE);
        editor = myshare.edit();
        menu=(ImageView)findViewById(R.id.menubar);
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(PaymentsActivty2.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {


            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(PaymentsActivty2.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(PaymentsActivty2.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(PaymentsActivty2.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(PaymentsActivty2.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(PaymentsActivty2.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(PaymentsActivty2.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(PaymentsActivty2.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(PaymentsActivty2.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(PaymentsActivty2.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(PaymentsActivty2.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        Payment_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detalis = new Intent(PaymentsActivty2.this,PaymentingActivity2.class);
                startActivity(detalis);
            }
        });
    }
}
