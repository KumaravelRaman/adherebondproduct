package com.brainmagic.adherebond;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.adherebond.R;

import java.util.List;

import Logout.logout;
import about.About_Activity;
import adapter.CurrentProjectDetailsAdapter;
import alertbox.Alertbox;
import api.models.project.Project;
import api.models.project.ProjectData;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import search.Search_Activity;

public class ProjectActivity extends AppCompatActivity {
    private ImageView onbackpressed,menu;
    private ListView current;
    ImageView home,back;
    private Alertbox box = new Alertbox(ProjectActivity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private ListView lvcurrent;
    private CurrentProjectDetailsAdapter currentProjectDetailsAdapter;
    List<Project> Data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        myshare = getSharedPreferences("Registration", ProjectActivity.this.MODE_PRIVATE);
        editor = myshare.edit();
        current=(ListView)findViewById(R.id.lvcurrent) ;
        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        menu=(ImageView)findViewById(R.id.menubar);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView mTitle_text = findViewById(R.id.title_text);
        mTitle_text.setText("Projects");

        menu.setOnClickListener(new View.OnClickListener() {


            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(ProjectActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(ProjectActivity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(ProjectActivity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(ProjectActivity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(ProjectActivity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(ProjectActivity.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(ProjectActivity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(ProjectActivity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(ProjectActivity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(ProjectActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(ProjectActivity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });

        checkiInternet();



    }
    private void checkiInternet() {
        NetworkConnection isnet = new NetworkConnection(ProjectActivity.this);
        if (isnet.CheckInternet()) {
            GetMembersActivity();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }

    }

    private void GetMembersActivity() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ProjectActivity.this, "Adhere Bonds", "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<ProjectData> call = service.current1();
            call.enqueue(new Callback<ProjectData>() {
                @Override
                public void onResponse(Call<ProjectData> call, Response<ProjectData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            Data = response.body().getData();
                            if(Data.size() ==0){
                                box.showAlertbox(getResources().getString(R.string.Data));
                            }else{
                                currentProjectDetailsAdapter = new CurrentProjectDetailsAdapter(ProjectActivity.this, Data);
                                current.setAdapter(currentProjectDetailsAdapter);
                            }

                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.v("GetServiceComplaints", e.getMessage());
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<ProjectData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
