package com.brainmagic.adherebonds.demo;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.brainmagic.adherebond.Login_Activity;
import com.brainmagic.adherebond.ProjectActivity;
import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Welcome_Activity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Logout.logout;
import about.About_Activity;
import adapter.ViewMobileOrder;
import alertbox.Alertbox;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import persistency.ServerConnection;
import product.Product_Activity;
import search.Search_Activity;

public class New_Mobile_Orderstatus_Activity extends AppCompatActivity {
    static EditText FromDate,Todate;
    Button Search;
    private ImageView menu;
    String customerstring,cuscode,statusstring;
    String fromstring,tostring;
    private ProgressDialog progressDialog;
    private Connection connection;
    private Statement stmt;
    private ResultSet rset;
    private ArrayList<String> CustomerNameList,Productnamelist,DateList,OrderIdlist,PackList,QtyList,SpinnerStatusList,StatusList,modelist,numberlist,colorlist,discountlist;
    private Spinner Dealerspinner,Statusspinner;
    //private ArrayList<String> DealerNamesList,DealerCodeList;
    private HorizontalScrollView Sales_horizontal;
    private ListView Sales_list;
    private String CustomerName;
    private SQLiteDatabase db,db2;

    private Alertbox box = new Alertbox(New_Mobile_Orderstatus_Activity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    String salesID,SalesName,TableName;
    ImageView home,back;
    Integer intdays;
    String Customername,Fitertype,stringdays;


    DateFormat parser = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new__mobile__orderstatus_);
        Dealerspinner  = (Spinner) findViewById(R.id.customer_spinner);
        Statusspinner  = (Spinner) findViewById(R.id.status_spinner);
        //DealerCodeList = new ArrayList<String>();
        menu=(ImageView)findViewById(R.id.menubar);
        SpinnerStatusList  =  new ArrayList<String>();
        //DealerNamesList  =  new ArrayList<String>();
        // DealerNamesList.add("All");
        SpinnerStatusList.add("All");
        SpinnerStatusList.add("Order Pending");
        SpinnerStatusList.add("Invoice Generated");
        SpinnerStatusList.add("Dispatched");
        Sales_list = (ListView) findViewById(R.id.sales_list);

        StatusList  =  new ArrayList<String>();
        CustomerNameList  =  new ArrayList<String>();
        Productnamelist  =  new ArrayList<String>();
        DateList  =  new ArrayList<String>();
        OrderIdlist  =  new ArrayList<String>();
        PackList  =  new ArrayList<String>();
        QtyList  =  new ArrayList<String>();
        modelist  =  new ArrayList<String>();
        numberlist  =  new ArrayList<String>();
        colorlist  =  new ArrayList<String>();
        discountlist=new ArrayList<>();

        Sales_horizontal = ((HorizontalScrollView)findViewById(R.id.sales_horizontal));

        myshare = getSharedPreferences("Registration", New_Mobile_Orderstatus_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();
        SalesName = myshare.getString("name", "");
        progressDialog = new ProgressDialog(New_Mobile_Orderstatus_Activity.this);

        home = (ImageView) findViewById(R.id.home);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(New_Mobile_Orderstatus_Activity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        FromDate = (EditText)findViewById(R.id.fromdate);
        Todate = (EditText)findViewById(R.id.todate);

        FromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new New_Mobile_Orderstatus_Activity.FromDatePickerFragment();
                newFragment.show(getSupportFragmentManager(),"datePicker");

            }

        });
        Todate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new New_Mobile_Orderstatus_Activity.ToDatePickerFragment();
                newFragment.show(getSupportFragmentManager(),"datePicker");
                Todate.clearFocus();
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(New_Mobile_Orderstatus_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(New_Mobile_Orderstatus_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(New_Mobile_Orderstatus_Activity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(New_Mobile_Orderstatus_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(New_Mobile_Orderstatus_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(New_Mobile_Orderstatus_Activity.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(New_Mobile_Orderstatus_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(New_Mobile_Orderstatus_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(New_Mobile_Orderstatus_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(New_Mobile_Orderstatus_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        Search = (Button)findViewById(R.id.search);
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromstring = FromDate.getText().toString();
                tostring = Todate.getText().toString();
                statusstring = Statusspinner.getSelectedItem().toString();
                //customerstring = Dealerspinner.getSelectedItem().toString();
                if(FromDate.getText().toString().equals("") && Todate.getText().toString().equals(""))
                {
                    if(statusstring.equals("All"))
                    {
                        new GetTotalDetails().execute();
                    }
                    else
                    {
                        new GetStatusDetails().execute();
                    }

                }
                else {
                    if(FromDate.getText().toString().equals(""))
                    {
                        Toast.makeText(getApplicationContext(),"Enter From date",Toast.LENGTH_SHORT).show();
                    } else if(Todate.getText().toString().equals(""))
                    {
                        Toast.makeText(getApplicationContext(),"Enter To date",Toast.LENGTH_SHORT).show();
                    } else {
                        if (statusstring.equals("All")) {
                            new GetTotalStatusWithdate().execute();
                        } else {
                            new GetStatusWithdate().execute();
                        }
                    }
                }
            }
        });

        GetSpinner();
    }

    public static class FromDatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            //FromDate.setText(day + "/" + (month + 1) + "/" + year);
            FromDate.setText(year + "-" + (month + 1) + "-" + day);
            FromDate.clearFocus();
        }
    }
    public static class ToDatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            Todate.setText(year + "-" + (month + 1) + "-" + day);
            Todate.clearFocus();
        }
    }



    private class GetTotalDetails extends AsyncTask<String, Void, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ServerConnection server = new ServerConnection();
            CustomerNameList.clear();
            OrderIdlist.clear();
            Productnamelist.clear();
            StatusList.clear();
            PackList.clear();
            QtyList.clear();
            DateList.clear();
            try {
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select * from mobile_orders_new where executivename = '" + SalesName + "' order by date asc";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                if(rset.next())
                {
                    do {
                        CustomerNameList.add(capitalizeString(rset.getString("dealername")));
                        OrderIdlist.add(rset.getString("orderid"));
                        Productnamelist.add(capitalizeString(rset.getString("productname")));
                        StatusList.add(rset.getString("status"));
                        PackList.add(rset.getString("package"));
                        QtyList.add(rset.getString("quantity"));
                        DateList.add(rset.getString("date"));
                        modelist.add(rset.getString("OrderMode"));
                        numberlist.add(rset.getString("Number"));
                        colorlist.add(rset.getString("Color"));
                        discountlist.add(rset.getString("discount"));
                    }while (rset.next());
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "success";
                }
                else {
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "nodata";
                }

            } catch (Exception e) {
                Log.v("list", e.getMessage());
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {

                ViewMobileOrder Adapter = new ViewMobileOrder(New_Mobile_Orderstatus_Activity.this, CustomerNameList,OrderIdlist,Productnamelist,StatusList,PackList,QtyList,DateList,modelist,numberlist,colorlist,discountlist);
                Sales_list.setAdapter(Adapter);
            } else if(result.equals("nodata"))
            {
                box.showAlertbox("No Project Found");
                Sales_list.setAdapter(null);
            }else if(result.equals("error")){
                box.showAlertbox("Internet Connection is Slow");
                Sales_list.setAdapter(null);
            }
        }
    }
    private class GetStatusDetails extends AsyncTask<String, Void, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            CustomerNameList.clear();
            OrderIdlist.clear();
            Productnamelist.clear();
            StatusList.clear();
            PackList.clear();
            QtyList.clear();
            DateList.clear();

            ServerConnection server = new ServerConnection();
            try {
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select * from mobile_orders_new where status = '" + statusstring + "' AND executivename = '" + SalesName + "' order by date asc";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                if(rset.next())
                {
                    do {
                        CustomerNameList.add(capitalizeString(rset.getString("dealername")));
                        OrderIdlist.add(rset.getString("orderid"));
                        Productnamelist.add(capitalizeString(rset.getString("productname")));
                        StatusList.add(rset.getString("status"));
                        PackList.add(rset.getString("package"));
                        QtyList.add(rset.getString("quantity"));
                        DateList.add(rset.getString("date"));
                        modelist.add(rset.getString("OrderMode"));
                        numberlist.add(rset.getString("Number"));
                        colorlist.add(rset.getString("Color"));
                        discountlist.add(rset.getString("discount"));
                    }while (rset.next());
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "success";
                }
                else {
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "nodata";
                }

            } catch (Exception e) {
                Log.v("list", e.getMessage());
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                ViewMobileOrder Adapter = new ViewMobileOrder(New_Mobile_Orderstatus_Activity.this, CustomerNameList,OrderIdlist,Productnamelist,StatusList,PackList,QtyList,DateList,modelist,numberlist,colorlist,discountlist);
                Sales_list.setAdapter(Adapter);


            } else if(result.equals("nodata"))
            {
                box.showAlertbox("No Project Found");
                Sales_list.setAdapter(null);

            }else if(result.equals("error")){
                box.showAlertbox("Internet Connection is Slow");
                Sales_list.setAdapter(null);

            }
        }
    }


    private class GetTotalStatusWithdate extends AsyncTask<String, Void, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ServerConnection server = new ServerConnection();
            CustomerNameList.clear();
            OrderIdlist.clear();
            Productnamelist.clear();
            StatusList.clear();
            PackList.clear();
            QtyList.clear();
            DateList.clear();
            DateList.clear();
            try {
                connection = server.getConnection();
                stmt = connection.createStatement();
                String query = "select * from mobile_orders_new  WHERE date BETWEEN '" + fromstring + "' AND '"+ tostring + "' AND executivename = '" + SalesName + "' order by date asc";
                Log.v("Query", query);
                rset = stmt.executeQuery(query);
                if (rset.next())
                {
                    do{
                        CustomerNameList.add(capitalizeString(rset.getString("dealername")));
                        OrderIdlist.add(rset.getString("orderid"));
                        Productnamelist.add(capitalizeString(rset.getString("productname")));
                        StatusList.add(rset.getString("status"));
                        PackList.add(rset.getString("package"));
                        QtyList.add(rset.getString("quantity"));
                        DateList.add(rset.getString("date"));
                        modelist.add(rset.getString("OrderMode"));
                        numberlist.add(rset.getString("Number"));
                        colorlist.add(rset.getString("Color"));
                        discountlist.add(rset.getString("discount"));

                    }while (rset.next());
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "success";
                }
                else {
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "nodata";
                }

            } catch (Exception e) {
                Log.v("list", e.getMessage());
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                ViewMobileOrder Adapter = new ViewMobileOrder(New_Mobile_Orderstatus_Activity.this, CustomerNameList,OrderIdlist,Productnamelist,StatusList,PackList,QtyList,DateList,modelist,numberlist,colorlist,discountlist);
                Sales_list.setAdapter(Adapter);


            } else if(result.equals("nodata"))
            {
                box.showAlertbox("No Project Found");
                Sales_list.setAdapter(null);

            }else if(result.equals("error")){
                box.showAlertbox("Internet Connection is Slow");
                Sales_list.setAdapter(null);

            }
        }
    }


    private class GetStatusWithdate extends AsyncTask<String, Void, String>
    {
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ServerConnection server = new ServerConnection();

            CustomerNameList.clear();
            OrderIdlist.clear();
            Productnamelist.clear();
            StatusList.clear();
            PackList.clear();
            QtyList.clear();
            DateList.clear();
            try {
                connection = server.getConnection();
                stmt = connection.createStatement();

                String query = "select * from mobile_orders_new WHERE date BETWEEN '" + fromstring + "' AND '" + tostring + "' AND status = '" + statusstring + "' AND executivename = '" + SalesName + "' order by date asc";
                Log.v("Query GetAgingWithdate ", query);
                rset = stmt.executeQuery(query);
                if(rset.next())
                {
                    do {
                        CustomerNameList.add(capitalizeString(rset.getString("dealername")));
                        OrderIdlist.add(rset.getString("orderid"));
                        Productnamelist.add(capitalizeString(rset.getString("productname")));
                        StatusList.add(rset.getString("status"));
                        PackList.add(rset.getString("package"));
                        QtyList.add(rset.getString("quantity"));
                        DateList.add(rset.getString("date"));
                        modelist.add(rset.getString("OrderMode"));
                        numberlist.add(rset.getString("Number"));
                        colorlist.add(rset.getString("Color"));
                        discountlist.add(rset.getString("discount"));
                    }while (rset.next());
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "success";
                }
                else {
                    connection.close();
                    stmt.close();
                    rset.close();
                    return "nodata";
                }

            } catch (Exception e) {
                Log.v("list", e.getMessage());
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result.equals("success")) {
                ViewMobileOrder Adapter = new ViewMobileOrder(New_Mobile_Orderstatus_Activity.this, CustomerNameList,OrderIdlist,Productnamelist,StatusList,PackList,QtyList,DateList,modelist,numberlist,colorlist,discountlist);
                Sales_list.setAdapter(Adapter);


            } else if(result.equals("nodata"))
            {
                box.showAlertbox("No Project Found");
                Sales_list.setAdapter(null);

            }else if(result.equals("error")){
                box.showAlertbox("Internet Connection is Slow");
                Sales_list.setAdapter(null);

            }
        }
    }



    void GetSpinner()
    {
        ArrayAdapter<String> StatusAdapter = new ArrayAdapter<String>(New_Mobile_Orderstatus_Activity.this, R.layout.simple_spinner_item, SpinnerStatusList);
        StatusAdapter.setNotifyOnChange(true);
        StatusAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        Statusspinner.setAdapter(StatusAdapter);
    }

            /*private class GetDealerNamesForSpinner extends AsyncTask<String, Void, String>
            {
                DBHelpersync dbhelper =new DBHelpersync(Mobile_Orderstatus_Activity.this);
                Cursor cursor;
                protected void onPreExecute() {
                    super.onPreExecute();
                *//*  progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();*//*
                    db = dbhelper.getWritableDatabase();

                }
                @Override
                protected String doInBackground(String... params)
                {
                    // TODO Auto-generated method stub
                    Log.v("OFF Line", "SQLITE Table");
                    try
                    {
                        String query ="select distinct Name,CustomerCode from Customer where SalesPersonName='"+SalesName+"' order by Name asc";
                        Log.v("Dealer Name",query);
                        cursor = db.rawQuery(query, null);
                        if(cursor.moveToFirst()) {
                            do {
                                DealerNamesList.add(capitalizeString(cursor.getString(cursor.getColumnIndex("Name"))));
                                DealerCodeList.add(cursor.getString(cursor.getColumnIndex("CustomerCode")));
                            } while (cursor.moveToNext());
                            cursor.close();
                            db.close();
                            return "received";
                        } else {
                            cursor.close();
                            db.close();
                            return "nodata";
                        }
                    }catch (Exception e)
                    {
                        cursor.close();
                        db.close();
                        Log.v("Error in get date","catch is working");
                        Log.v("Error in Incentive", e.getMessage());
                        return "notsuccess";
                    }
                }
                @Override
                protected void onPostExecute(String result) {
                    super.onPostExecute(result);
                    // progressDialog.dismiss();

                    switch (result) {
                        case "received":
                            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(Mobile_Orderstatus_Activity.this, R.layout.simple_spinner_item, DealerNamesList);
                            spinnerAdapter.setNotifyOnChange(true);
                            spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                            Dealerspinner.setAdapter(spinnerAdapter);
                            ArrayAdapter<String> StatusAdapter = new ArrayAdapter<String>(Mobile_Orderstatus_Activity.this, R.layout.simple_spinner_item, SpinnerStatusList);
                            StatusAdapter.setNotifyOnChange(true);
                            StatusAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
                            Statusspinner.setAdapter(StatusAdapter);
                            break;
                        case "nodata":
                            box.showAlertbox("No Customers Found");
                            break;
                        default:
                            box.showAlertbox("Error in Customer List");
                            break;
                    }

                }

            }*/

    public String method(String str) {
        if (str != null && str.length() > 0) {
            str = str.substring(0, str.length()-7);
        }
        return str;
    }

    public static String capitalizeString(String string ) {
        try
        {
            char[] chars = string.toLowerCase().toCharArray();
            boolean found = false;
            for (int i = 0; i < chars.length; i++) {
                if (!found && Character.isLetter(chars[i])) {
                    chars[i] = Character.toUpperCase(chars[i]);
                    found = true;
                } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '&') { // You can add other chars here
                    found = false;
                }
            }
            return String.valueOf(chars);
        }catch (Exception e)
        {
            e.printStackTrace();
            return "error";
        }
    }

           /* void  totalamout()    {
                double sum = 0;
                for(int j=0; j< AmountList.size(); j++){
                    sum += Double.parseDouble(AmountList.get(j));
                }
                Amount.setText(String.format("%.2f",  + sum));
            }*/
}

