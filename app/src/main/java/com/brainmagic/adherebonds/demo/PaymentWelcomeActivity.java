package com.brainmagic.adherebond;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.adherebond.R;

import Logout.logout;
import SQL_Fields.SQLITE;
import about.About_Activity;
import alertbox.Alertbox;
import alertbox.PasswordChangeAlert;
import api.models.master.Master;
import api.models.master.MasterData;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import contact.Contact_Activity;
import database.DBHelpersync;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import product.Product_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import roomdb.database.AppDatabase;
import search.Search_Activity;

import static roomdb.database.AppDatabase.getAppDatabase;

public class PaymentWelcomeActivity extends AppCompatActivity {
    private NetworkConnection network = new NetworkConnection(PaymentWelcomeActivity.this);
    private Alertbox alert = new Alertbox(PaymentWelcomeActivity.this);
    private AppDatabase db;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private PopupWindow mPopupWindow;
    ImageView home, back, menu;
    RelativeLayout email_layout;
    private String SalesName = "", salesID = "";
    private RelativeLayout mRelativeLayout;
    private View customView;
    private TextView temp1, temp2, temp3,temp4,temp5,Welcome,temp6;
    LinearLayout compose, showcompose, template,schemes;
    RelativeLayout sychronizelayout,  change_passwordlayout,  Customerlist_layout,Instructions_Layout,Uploadlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_welcome);
        myshare = getSharedPreferences("Registration", Context.MODE_PRIVATE);
        editor = myshare.edit();
        TextView mTitle_text = findViewById(R.id.title_text);
        mTitle_text.setText("Welcome");
        db = getAppDatabase(PaymentWelcomeActivity.this);

        back = (ImageView) findViewById(R.id.back);
        home = (ImageView) findViewById(R.id.home);
      //  template = (LinearLayout) findViewById(R.id.template);
       // compose = (LinearLayout) findViewById(R.id.compose);
       // showcompose = (LinearLayout) findViewById(R.id.showcompose);
      //  email_layout = (RelativeLayout) findViewById(R.id.emaillayout);
      //  mRelativeLayout = (RelativeLayout) findViewById(R.id.activity_search_gps);
        change_passwordlayout = (RelativeLayout) findViewById(R.id.change_password);
       // Uploadlayout = (RelativeLayout) findViewById(R.id.upload);
        menu=(ImageView)findViewById(R.id.menubar);
        // Initialize a news instance of LayoutInflater service
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        // Inflate the custom layout/view
        customView = inflater.inflate(R.layout.template_popup, null);
        menu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                final PopupMenu popupMenu = new PopupMenu(PaymentWelcomeActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }

                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub

                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(PaymentWelcomeActivity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.productmenu:
                                Intent product = new Intent(PaymentWelcomeActivity.this, Product_Activity.class);
                                startActivity(product);
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(PaymentWelcomeActivity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(PaymentWelcomeActivity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.watsnew:
                                Intent contact1 = new Intent(PaymentWelcomeActivity.this, ProjectActivity.class);
                                startActivity(contact1);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(PaymentWelcomeActivity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {

                                    Intent b = new Intent(PaymentWelcomeActivity.this, PaymentWelcomeActivity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));

                                    Intent b = new Intent(PaymentWelcomeActivity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(PaymentWelcomeActivity.this).log_out();
                                return true;
                        }
                        return false;
                    }

                });

                popupMenu.inflate(R.menu.popupmenu_login_inner);
                //Whatsnew_Activity.super.onPrepareOptionsMenu(popupMenu);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();

            }
        });
        // Initialize a news instance of popup window
        mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }

        // Get a reference for the custom view close button
        ImageButton closeButton = (ImageButton) customView.findViewById(R.id.ib_close);

        // Set a click listener for the popup window close button
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                mPopupWindow.dismiss();
            }
        });


        /*email_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(showcompose.getVisibility() == View.VISIBLE)
                    showcompose.setVisibility(View.GONE);
                else
                    showcompose.setVisibility(View.VISIBLE);
            }
        });*/

        /*compose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(PaymentWelcomeActivity.this, Email_Activity.class);
                startActivity(a);
                showcompose.setVisibility(View.GONE);
            }
        });
*/

       /* template.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Finally, show the popup window at the center location of root relative layout
                mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);

                temp1 = (TextView) customView.findViewById(R.id.temp1);
                temp2 = (TextView) customView.findViewById(R.id.temp2);
                temp3 = (TextView) customView.findViewById(R.id.temp3);
                temp4 = (TextView) customView.findViewById(R.id.temp4);
                temp5 = (TextView) customView.findViewById(R.id.temp5);
                temp6 = (TextView) customView.findViewById(R.id.temp6);

                temp1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showEmailTemp("PDC Cheque", getString(R.string.template1));

                    }
                });

                temp2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showEmailTemp("Cheque Receipt", getString(R.string.template2));

                    }
                });

                temp3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showEmailTemp("PDC Reminder", getString(R.string.template3));

                    }
                });
                temp4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showEmailTemp("Cheque Bounce",getString(R.string.template4) );

                    }
                });
                temp5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showEmailTemp("Dispatch", getString(R.string.template5));

                    }
                });
                temp6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showEmailTemp("cash receipt ", getString(R.string.template6));

                    }
                });
            }
        });*/
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(PaymentWelcomeActivity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(PaymentWelcomeActivity.this, Home_Activity.class);
                a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }

        });

        change_passwordlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckInternetforpass();
            }
        });
       /* Uploadlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InternetConnection();


            }
        });*/


    }

    void InternetConnection()
    {
        NetworkConnection network = new NetworkConnection(PaymentWelcomeActivity.this);
        if (network.CheckInternet()) {
            final AlertDialog alertDialog = new AlertDialog.Builder(PaymentWelcomeActivity.this).create();

            LayoutInflater inflater = getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.order_alertbox, null);
            alertDialog.setView(dialogView);
            TextView existing = (TextView) dialogView.findViewById(R.id.text2);
            TextView newcustomer = (TextView) dialogView.findViewById(R.id.text3);


            existing.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub
                    alertDialog.dismiss();
                    String a="order";
                    try{
                        if(!isOrderTableExists()) {
                            alert.showAlertbox("No Orders Found");
                        }else if(!isDataOrderTableExists())
                            alert.showAlertbox("No Orders Found");
                        else
                        {
                            Intent n = new Intent(PaymentWelcomeActivity.this, Preview_Activity.class);
                            // n.putExtra("a",a);
                            startActivity(n);
                        }
                    }catch (Exception e)
                    {
                        Log.v("Exception in Order",e.getMessage());
                    }
                }

            });

            newcustomer.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    alertDialog.dismiss();
                    String a="neworder";
                    // TODO Auto-generated method stub
                    if(!isNewOrderTableExists()) {
                        alert.showAlertbox("No Orders Found");
                    }else if(!isDataNewOrderTableExists())
                        alert.showAlertbox("No Orders Found");
                    else
                        startActivity(new Intent(PaymentWelcomeActivity.this, Preview_New_Activity.class));
                }

            });

            alertDialog.show();

        }
        else {
            Alertbox alert = new Alertbox(PaymentWelcomeActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }
    public boolean isOrderTableExists() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(PaymentWelcomeActivity.this);

        mDatabase = dbHelp.getWritableDatabase();
        //String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = 'Orderpreview'";
        String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '"+ SQLITE.TABLE_Additems+"'";
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;
    }
    public boolean isDataOrderTableExists() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(PaymentWelcomeActivity.this);

        mDatabase = dbHelp.getWritableDatabase();
        String query = "select * from "+SQLITE.TABLE_Additems;
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;
    }
    public boolean isNewOrderTableExists() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(PaymentWelcomeActivity.this);

        mDatabase = dbHelp.getWritableDatabase();
        //String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = 'NewOrderpreview'";
        String query = "select DISTINCT tbl_name from sqlite_master where tbl_name = '"+SQLITE.TABLE_NewPartsOrder+"'";
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;
    }


    public boolean isDataNewOrderTableExists() {
        SQLiteDatabase mDatabase;
        DBHelpersync dbHelp = new DBHelpersync(PaymentWelcomeActivity.this);
        mDatabase = dbHelp.getWritableDatabase();
        String query = "select * from "+SQLITE.TABLE_NewPartsOrder;
        Cursor cursor = mDatabase.rawQuery(query, null);
        Log.v("preview",query);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                mDatabase.close();
                return true;
            }
            cursor.close();
            mDatabase.close();
        }
        return false;
    }

    public void Onsync(View view) {
        if (network.CheckInternet()) {
            GetAllMasterData();
        } else {
            alert.showAlertbox(getString(R.string.no_internet));
        }
    }

    public void OnviewCustomer(View view) {
        /*if (myshare.getBoolean("synchronize",false)) {

            Intent f = new Intent(PaymentWelcomeActivity.this, ViewCustomerPaymentActivity.class);
            startActivity(f);
        }else{
            alert.showAlertbox("Kindly download data to view Customers!");
        }*/
        Intent f = new Intent(PaymentWelcomeActivity.this, ViewCustomerPaymentActivity.class);
        startActivity(f);
    }

    public void showEmailTemp(final String subject, final String body) {
        final AlertDialog alertDialog = new AlertDialog.Builder(PaymentWelcomeActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.temp_alert, null);
        alertDialog.setView(dialogView);

        TextView subtxt = (TextView) dialogView.findViewById(R.id.subjectedit);
        TextView bodytxt = (TextView) dialogView.findViewById(R.id.bodyedit);
        Button selectbtn = (Button) dialogView.findViewById(R.id.select);
        Button cancelbtn = (Button) dialogView.findViewById(R.id.cancel);
        subtxt.setText(subject);
        bodytxt.setText(body);
        selectbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent emailIntent = new Intent(PaymentWelcomeActivity.this, Email_Activity.class);
                emailIntent.putExtra("subject", subject);
                emailIntent.putExtra("body", body);
                startActivity(emailIntent);
                mPopupWindow.dismiss();
                alertDialog.dismiss();
                showcompose.setVisibility(View.GONE);

            }
        });

        cancelbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void CheckInternetforpass() {
        NetworkConnection network = new NetworkConnection(PaymentWelcomeActivity.this);
        if (network.CheckInternet()) {
            PasswordChangeAlert alert = new PasswordChangeAlert(PaymentWelcomeActivity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            Alertbox alert = new Alertbox(PaymentWelcomeActivity.this);
            alert.showAlertbox("Kindly check your Internet Connection");
        }

    }


    private void GetAllMasterData() {

        try {
            final ProgressDialog loading = ProgressDialog.show(PaymentWelcomeActivity.this, getResources().getString(R.string.app_launcher_name), "Downloading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<Master> call = service.GetMasterData(myshare.getString("name", "").toString());
            //  Call<Master> call = service.GetMasterData("Siva");
            call.enqueue(new Callback<Master>() {
                @Override
                public void onResponse(Call<Master> call, Response<Master> response) {
                    try {
                        loading.dismiss();

                        if (response.body().getResult().equals("Success") || response.body().getResult().equals("OnlyCustomer"))
                            SaveMasterData(response.body().getData());

                        else if (response.body().getResult().equals("NoCustomer"))
                            alert.showAlertbox("Customer data not available");

                        else
                            alert.showAlertbox(getResources().getString(R.string.server_error));

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alert.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }

                @Override
                public void onFailure(Call<Master> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alert.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void SaveMasterData(MasterData data) {

        db.adhereDao().DeleteAllCustomer();
        db.adhereDao().DeleteAllAgeing();
        db.adhereDao().DeleteAllPayments();

        db.adhereDao().insertAllCustomer(data.getCustomer_list());
        // db.adhereDao().insertAllAgeing(data.getAmt_list());
        db.adhereDao().insertAllPaymets(data.getPAl_list());
        editor.putBoolean("synchronize",true);
        editor.commit();
        //  alert.showAlertbox(db.adhereDao().GetCustomer_list().get(0).getName());
        alert.showAlertbox("Master data downloaded Successfully");
        // alert.showAlertbox();

    }
    public void Payments(View view) {
       /* if (myshare.getBoolean("synchronize",false)) {


        }
        else{
            alert.showAlertbox("Kindly download data to view Payments!");
        }*/
        startActivity(new Intent(PaymentWelcomeActivity.this, PaymentsActivty2.class));

    }

    public void Schemes(View view) {
        startActivity(new Intent(PaymentWelcomeActivity.this, Offers_Activity.class));
    }

    }

