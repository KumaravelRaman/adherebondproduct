package com.brainmagic.adherebonds;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.RegisterActivity;

import home.Home_Activity;

public class Splash_two_Activity extends AppCompatActivity {

    private static final long SPLASH_DISPLAY_LENGTH = 1500;
    Boolean isregister;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_two_);
        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        isregister = myshare.getBoolean("isregister",false);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                /* Create an Intent that will start the Menu-Activity. */
                if (!isregister)
                {
                    Intent mainIntent = new Intent(Splash_two_Activity.this, RegisterActivity.class);
                    startActivity(mainIntent);
                    finish();
                }
                else {
                    Intent mainIntent = new Intent(Splash_two_Activity.this, Home_Activity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mainIntent);
                    finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
