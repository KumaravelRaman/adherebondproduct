package database;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import SQL_Fields.SQLITE;

//DB helper for synchronized tables Customer details, Aging, TrialBalance, PDC Table
public class DBHelperSales extends SQLiteOpenHelper {
	public Context context;
	private SQLiteDatabase sqLiteDatabase;
	private static final int DATABASE_VERSION = 1;

	@SuppressLint({"SdCardPath"})
	public DBHelperSales(Context context) {

		super(context, SQLITE.DATABASE_NAME, null, DATABASE_VERSION);
		this.sqLiteDatabase = null;
		this.context = context;
	}

	public void onCreate(SQLiteDatabase sqLiteDatabase) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

	}

}

