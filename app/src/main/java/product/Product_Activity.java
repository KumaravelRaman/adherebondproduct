package product;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Login_Activity;
import com.brainmagic.adherebond.ProjectActivity;
//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Splash_two_Activity;
import com.brainmagic.adherebond.Welcome_Activity;

import java.util.ArrayList;

import Logout.logout;
import about.About_Activity;
import adapter.Catagory_Adapter;
import adapter.ImageAdapter;
import alertbox.Alertbox;
import alertbox.LoginAlert;
import alertbox.PasswordChangeAlert;
import contact.Contact_Activity;
import database.DBHelper;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import search.Search_Activity;
import whatsnew.Whatsnew_Activity;

public class Product_Activity extends AppCompatActivity {


    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    NetworkConnection network = new NetworkConnection(Product_Activity.this);
    Alertbox alert = new Alertbox(Product_Activity.this);
    ArrayList<String> catagoryList, imageList,PriorityList;
    ImageView back,home,menu;
    ProgressDialog progressDialog;
    private ArrayList<String> IDList;
    private ArrayList<String> SUBIDList;
    private String a="";
    String usertype="";
    private ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        myshare = getSharedPreferences("Registration",Product_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();

        TextView mTitle_text = findViewById(R.id.title_text);
        mTitle_text.setText("Products");

        catagoryList = new ArrayList<>();
        imageList = new ArrayList<>();
        IDList = new ArrayList<>();
        PriorityList = new ArrayList<>();
        SUBIDList = new ArrayList<>();
        listview = (ListView) findViewById(R.id.gridView);
        a=getIntent().getStringExtra("a");
        new GetCatagory().execute();
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent a=new Intent(Product_Activity.this,ProductList_Activity.class);
                a.putExtra("Category",catagoryList.get(i));
                a.putExtra("ID",IDList.get(i));
               // a.putExtra("SUBIDList",SUBIDList.get(i));
                startActivity(a);
            }
        });



        back=(ImageView)findViewById(R.id.back);
        home=(ImageView)findViewById(R.id.home);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(Product_Activity.this, Home_Activity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainIntent);
                finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        menu=(ImageView)findViewById(R.id.menubar);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Product_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub


                        switch (item.getItemId()) {
                            case R.id.aboutmenu:
                                Intent about = new Intent(Product_Activity.this, About_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.expense:
                                PasswordChangeAlert alerts = new PasswordChangeAlert(Product_Activity.this);// new saleslogin().execute();
                                alerts.showLoginbox();
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(Product_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(Product_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(Product_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.watsnew:
                                if (network.CheckInternet()) {
                                    Intent f = new Intent(Product_Activity.this, ProjectActivity.class);
                                    startActivity(f);}
                                else{
                                    Alertbox alert = new Alertbox(Product_Activity.this);
                                    alert.showAlertbox("Kindly check your Internet Connection");
                                }
                                return true;
                            case R.id.offers:
                                usertype=myshare.getString("Usertype","");
                                if(usertype.equals("Cera Employee"))
                                {
                                    if (!myshare.getBoolean("islogin",false))
                                    {
                                        CheckInternet();
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("alertlogin",false)));
                                    }
                                    else
                                    {
                                        if (network.CheckInternet()) {
                                            Intent f = new Intent(Product_Activity.this, Offers_Activity.class);
                                            startActivity(f);
                                        }
                                        else {

                                            alert.showAlertbox("Kindly check your Internet Connection");
                                        }

                                    }
                                }
                                else
                                {
                                    Intent f = new Intent(Product_Activity.this, Offers_Activity.class);
                                    startActivity(f);
                                }return true;




                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {
                                    //menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(Product_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {

                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                    ///menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(Product_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(Product_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popupmenu_products);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();


            }
        });

    }



    @Override
    public void onBackPressed() {

        if(a.equals(""))
        {
            Intent a=new Intent(Product_Activity.this,Home_Activity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
        else {

            super.onBackPressed();
        }
    }

    private void CheckInternet() {
        if (network.CheckInternet()) {
            LoginAlert alert= new LoginAlert(Product_Activity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }



    class GetCatagory extends AsyncTask<String, Void, String> {

        private String productquery;
        private SQLiteDatabase db;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Product_Activity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {

            try

            {
                DBHelper dbhelper = new DBHelper(Product_Activity.this);
                db = dbhelper.readDataBase();
              //  productquery = "select distinct Mprod_name,MProd_Img,MProd_ID,Mprod_priority,deleteflag from Mprodcategory where deleteflag = 'notdelete' order by Mprod_priority asc";
                productquery = "\n" +
                        "select distinct Mprodcategory.Mprod_name,Mprodcategory.MProd_Img,Mprodcategory.MProd_ID,Mprodcategory.Mprod_priority,Mprodcategory.deleteflag\n" +
                        " from Mprodcategory,Product_Sub_Category where Mprodcategory.deleteflag = 'notdelete' and Product_Sub_Category.MProd_ID = Mprodcategory.MProd_ID order by Mprodcategory.Mprod_priority asc\n";
                //select distinct Mprod_name,MProd_Img,MProd_ID,Mprod_priority,deleteflag from Mprodcategory where deleteflag = 'notdelete' order by Mprod_priority asc
                Log.v("Product Query",productquery);
                Cursor cursor = db.rawQuery(productquery, null);

                if (cursor.moveToFirst()) {
                    do {

                        catagoryList.add(cursor.getString(cursor.getColumnIndex("Mprod_name")));
                        imageList.add(cursor.getString(cursor.getColumnIndex("MProd_Img")));
                        IDList.add(cursor.getString(cursor.getColumnIndex("MProd_ID")));
                       // SUBIDList.add(cursor.getString(cursor.getColumnIndex("MProdsub_ID")));
                        PriorityList.add(cursor.getString(cursor.getColumnIndex("Mprod_priority")));

                    }
                    while (cursor.moveToNext());
                    cursor.close();
                    return "success";
                } else {
                    return "empty";
                }



            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();

            switch (s) {
                case "success":
                    listview.setAdapter(new Catagory_Adapter(Product_Activity.this, catagoryList, imageList));
                    //listview.setAdapter(news CategoryAdapter(Product_Activity.this, catagoryList, imageList));
                    break;
                case "empty":
                    break;
                default:
                    break;
            }

        }
    }

}

