package adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


//import com.brainmagic.adherebond.R;

import com.brainmagic.adherebond.R;

import org.w3c.dom.NameList;

import java.util.ArrayList;
import java.util.List;

import api.models.master.Customer_list;

/**
 * Created by system01 on 2/12/2017.
 */

public class CuslistAdapter extends ArrayAdapter<Customer_list> {

    private Context context;
    private List<String> Namelist,MobileList,EmailList;

    //private List<Customer_list> Cuslist;


    public CuslistAdapter(Context context,List<String>Namelist,List<String>MobileList,List<String>EmailList) {
        super(context, R.layout.view_adapter_customer);
        this.context = context;
        this.Namelist = Namelist;
        this.MobileList = MobileList;
        this.EmailList = EmailList;



    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.view_adapter_customer, parent, false);

            TextView Text1 = (TextView) convertView.findViewById(R.id.text1);
           // TextView Text3 = (TextView) convertView.findViewById(R.id.text3);
            TextView Text4 = (TextView) convertView.findViewById(R.id.text4);
            TextView Text5 = (TextView) convertView.findViewById(R.id.text5);
            //convertView.setTag(convertView);
            Text1.setText(Namelist.get(position));
            //Text3.setText(Cuslist.get(position).getBranchCode());
            Text4.setText(MobileList.get(position));
            Text5.setText(EmailList.get(position));

            //Text4.setTag(position);

        }

        return convertView;
    }
    @Override
    public int getCount() {
        return Namelist.size();
    }

}
