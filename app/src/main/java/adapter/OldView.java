package adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.brainmagic.adherebond.Preview_Activity;
import com.brainmagic.adherebond.R;

import java.util.List;

import api.models.ExistingOrder.request2.Exist_Orders_List;
import api.models.OrderView.DataItem;

public class OldView extends ArrayAdapter {

    Context context;
    List<DataItem> data;
    List<Exist_Orders_List> existOrderslist;

    public OldView(Preview_Activity context, List<DataItem> data) {
        super(context, R.layout.expenseview);
        this.context=context;
        this.data=data;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Exist_Orders_List existOrder = new Exist_Orders_List();
        existOrder.setExecutivename(data.get(position).getCustomername());

        return super.getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }
}
