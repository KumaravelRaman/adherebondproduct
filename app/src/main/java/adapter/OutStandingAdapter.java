package adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

//import com.brainmagic.adherebond.R;

import androidx.core.app.ActivityCompat;

import com.brainmagic.adherebond.R;

import java.util.List;

import api.models.master.Customer_list;
import api.models.master.PAl_list;

/**
 * Created by SYSTEM10 on 7/19/2018.
 */

public class OutStandingAdapter  extends ArrayAdapter<PAl_list> {
    private Context context;

    private List<PAl_list> Cuslist;


    public OutStandingAdapter(Context context, List<PAl_list> Cuslist) {
        super(context, R.layout.sales_outstandingcustomer, Cuslist);
        this.context = context;
        this.Cuslist = Cuslist;



    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.sales_outstandingcustomer, parent, false);

           // TextView Text1 = (TextView) convertView.findViewById(R.id.Customer_Code);
            TextView Text3 = (TextView) convertView.findViewById(R.id.Customer_Name);
            TextView Text4 = (TextView) convertView.findViewById(R.id.Trial_Balance);
           // TextView Text5 = (TextView) convertView.findViewById(R.id.text5);
            //convertView.setTag(convertView);

            Text3.setText(Cuslist.get(position).getPartyName());
            Text4.setText(Cuslist.get(position).getPendingAmount());
           // Text5.setText(Cuslist.get(position).getEmailId());

            //Text4.setTag(position);
            Text4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + Cuslist.get(position).getMobile()));
                    // callIntent.setNewOrder(Uri.parse(MobileList.get(position)));
                    //callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    context.startActivity(callIntent);
                }
            });
        }

        return convertView;
    }
}
