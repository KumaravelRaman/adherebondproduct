package adapter;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


//import com.brainmagic.adherebond.R;
import androidx.annotation.NonNull;

import com.brainmagic.adherebond.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

import api.models.project.Project;
import api.models.project.ProjectData;
import br.com.felix.imagezoom.ImageZoom;

//import static com.thefinestartist.utils.content.ContextUtil.getApplicationContext;


/**
 * Created by SYSTEM10 on 7/14/2018.
 */

public class CurrentProjectDetailsAdapter extends ArrayAdapter {
    private Context context;
    private List<Project> data;
    private String ImageURL = "http://adherebond.brainmagicllc.com/ImageUpload/";
    private Bitmap thunnil;


    public CurrentProjectDetailsAdapter(@NonNull Context context, List<Project> data) {
        super(context, R.layout.currentprojecttittle, data);
        this.context = context;
        this.data = data;
    }

    @SuppressLint("WrongConstant")
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final currentproject1 holder;

        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.currentprojecttittle, parent, false);

            holder = new currentproject1();

            holder.tittle = (TextView) convertView.findViewById(R.id.projecttitle);
            holder.tittledesc = (TextView) convertView.findViewById(R.id.projetdescription1);
            holder.image1 = (ImageZoom) convertView.findViewById(R.id.image11);
            holder.image2 = (ImageZoom) convertView.findViewById(R.id.image22);
            holder.image3 = (ImageZoom) convertView.findViewById(R.id.image33);
            holder.image4 = (ImageZoom) convertView.findViewById(R.id.image44);
            holder.image5 = (ImageZoom) convertView.findViewById(R.id.image55);


            holder.tittle.setText(data.get(position).getProjectName());
            holder.tittledesc.setText(data.get(position).getDescription());

            if (data.get(position).getImage1() != null){

                Picasso.with(context)
                        .load("http://adherebond.brainmagicllc.com/ImageUpload/" + data.get(position).getImage1().replaceAll(" ", "%20"))
                        .error(R.drawable.no_image)

                        .into(holder.image1);
            }


            else
                Picasso.with(context)
                        .load(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(holder.image1);


            if (data.get(position).getImage2() != null){

                Picasso.with(context)
                        .load("http://adherebond.brainmagicllc.com/ImageUpload/" + data.get(position).getImage2().replaceAll(" ", "%20"))
                        .error(R.drawable.no_image)
                        .into(holder.image2);
            }

            else
                Picasso.with(context)
                        .load(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(holder.image2);


            if (data.get(position).getImage3() != null){

                Picasso.with(context)
                        .load("http://adherebond.brainmagicllc.com/ImageUpload/" + data.get(position).getImage3().replaceAll(" ", "%20"))
                        .error(R.drawable.no_image)
                        .into(holder.image3);
            }

            else
                Picasso.with(context)
                        .load(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(holder.image3);

            if (data.get(position).getImage4() != null){
                Picasso.with(context)
                        .load("http://adherebond.brainmagicllc.com/ImageUpload/" + data.get(position).getImage4().replaceAll(" ", "%20"))
                        .error(R.drawable.no_image)
                        .into(holder.image4);
            }

            else
                Picasso.with(context)
                        .load(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(holder.image4);

            if (data.get(position).getImage5() != null){

                Picasso.with(context)
                        .load("http://adherebond.brainmagicllc.com/ImageUpload/" + data.get(position).getImage5().replaceAll(" ", "%20"))
                        .error(R.drawable.no_image)
                        .into(holder.image5);
            }

            else
                Picasso.with(context)
                        .load(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(holder.image5);




            convertView.setTag(holder);
        } else {
            holder = (currentproject1) convertView.getTag();
        }




        return convertView;
    }




    public void watchYoutubeVideo( String url){
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

    private class currentproject1 {

        public TextView tittle;
        public TextView tittledesc;
        public ImageZoom image1;
        public ImageZoom image2;
        public ImageZoom image3;
        public ImageZoom image4;
        public ImageZoom image5;


    }
}
