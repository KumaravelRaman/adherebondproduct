package adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by system01 on 12/30/2016.
 */

public class Catagory_Adapter extends BaseAdapter {

    private Context mContext;
    ArrayList<String> categories;
    ArrayList<String> imagelist;
    private LayoutInflater layoutInflator;

    public Catagory_Adapter(Context context, ArrayList<String> categories, ArrayList<String> imagelist) {
        //mContext = c;
        this.categories = categories;
        this.imagelist = imagelist;
        this.mContext = context;

    }

    public int getCount() {
        return categories.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a news ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        convertView = null;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_catagorylist, parent, false);
            TextView grid_text = (TextView) convertView.findViewById(R.id.grid_text);
            grid_text.setText(categories.get(position));
        }

        return convertView;
    }

    // references to our images
        /*private Integer[] mThumbIds = {
                R.drawable.sample_2
        };*/
}