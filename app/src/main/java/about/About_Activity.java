package about;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Login_Activity;
import com.brainmagic.adherebond.ProjectActivity;
//import com.brainmagic.adherebond.R;
import com.brainmagic.adherebond.Welcome_Activity;

import java.util.ArrayList;

import Logout.logout;
import alertbox.Alertbox;
import alertbox.LoginAlert;
import alertbox.PasswordChangeAlert;
import contact.Contact_Activity;
import enquiry.Enquiry_Activity;
import home.Home_Activity;
import network.NetworkConnection;
import offers.Offers_Activity;
import product.Product_Activity;
import search.Search_Activity;
import whatsnew.Whatsnew_Activity;

public class About_Activity extends AppCompatActivity {


    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    NetworkConnection network = new NetworkConnection(About_Activity.this);
    Alertbox alert = new Alertbox(About_Activity.this);
    ArrayList<String> catagoryList, imageList,PriorityList;
    ImageView back,home,menu;
    ProgressDialog progressDialog;
    String usertype="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_);

        myshare = getSharedPreferences("Registration", About_Activity.this.MODE_PRIVATE);
        editor = myshare.edit();

        TextView mTitle_text = findViewById(R.id.title_text);
        mTitle_text.setText("About Us");


        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });

        home=(ImageView)findViewById(R.id.home);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(About_Activity.this, Home_Activity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainIntent);
                finish();
            }
        });

        menu=(ImageView)findViewById(R.id.menubar);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(About_Activity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu pop) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // TODO Auto-generated method stub


                        switch (item.getItemId()) {
                            case R.id.productmenu:
                                Intent about = new Intent(About_Activity.this, Product_Activity.class);
                                startActivity(about);
                                return true;
                            case R.id.expense:
                                PasswordChangeAlert alert = new PasswordChangeAlert(About_Activity.this);// new saleslogin().execute();
                                alert.showLoginbox();
                                return true;
                            case R.id.searchmenu:
                                Intent search = new Intent(About_Activity.this, Search_Activity.class);
                                startActivity(search);
                                return true;
                            case R.id.contactmenu:
                                Intent contact = new Intent(About_Activity.this, Contact_Activity.class);
                                startActivity(contact);
                                return true;
                            case R.id.enquirymenu:
                                Intent enquiry = new Intent(About_Activity.this, Enquiry_Activity.class);
                                startActivity(enquiry);
                                return true;
                            case R.id.watsnew:
                                if (network.CheckInternet()) {
                                    Intent f = new Intent(About_Activity.this, ProjectActivity.class);
                                    startActivity(f);}
                                else{
                                    Alertbox alerts = new Alertbox(About_Activity.this);
                                    alerts.showAlertbox("Kindly check your Internet Connection");
                                }
                                return true;
                            case R.id.offers:
                                usertype=myshare.getString("Usertype","");
                                if(usertype.equals("Cera Employee"))
                                {
                                    if (!myshare.getBoolean("islogin",false))
                                    {
                                        CheckInternet();
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                        Log.v("is  Login ",Boolean.toString(myshare.getBoolean("alertlogin",false)));
                                    }
                                    else
                                    {
                                        if (network.CheckInternet()) {
                                            Intent f = new Intent(About_Activity.this, Offers_Activity.class);
                                            startActivity(f);
                                        }
                                        else {

//                                           alerts.showAlertbox("Kindly check your Internet Connection");
                                        }

                                    }
                                }
                                else
                                {
                                    Intent f = new Intent(About_Activity.this, Offers_Activity.class);
                                    startActivity(f);
                                }return true;




                            case R.id.loginmenu:
                                if (myshare.getBoolean("islogin",false))
                                {
                                    //menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(About_Activity.this, Welcome_Activity.class);
                                    startActivity(b);
                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                }
                                else
                                {

                                    Log.v("is  Login ",Boolean.toString(myshare.getBoolean("islogin",false)));
                                    ///menu.findViewById(R.id.loginmenu).setVisibility(View.VISIBLE);
                                    Intent b = new Intent(About_Activity.this, Login_Activity.class);
                                    b.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    startActivity(b);
                                }
                                return true;
                            case R.id.logoutmenu:
                                new logout(About_Activity.this).log_out();
                                return true;
                        }
                        return false;
                    }
                });
                popupMenu.inflate(R.menu.popupmenu_about);
                Menu pop = popupMenu.getMenu();
                if (myshare.getBoolean("islogin",false))
                    pop.findItem(R.id.logoutmenu).setVisible(true);
                else
                    pop.findItem(R.id.loginmenu).setVisible(true);
                popupMenu.show();


            }
        });


    }


    private void CheckInternet() {
        if (network.CheckInternet()) {
            LoginAlert alert= new LoginAlert(About_Activity.this);// new saleslogin().execute();
            alert.showLoginbox();
        } else {
            alert.showAlertbox("Kindly check your Internet Connection");
        }
    }

}
